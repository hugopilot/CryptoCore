# Beginners Tutorial for using CryptoCore
You are just starting with CryptoCore? Well, thanks for downloading in advance! 

I will brief you though this program step by step. 

## Table of contents
- Before we start
- Some defenitions
- A brief introduction to cryptography
- Compiling from source
- Using the CLI
- Which kind of encryption should I use?


## Before we start
A few rules to keep in mind during this tutorial
1. Things might get complex in this tutorial, but it is very important that **you never give up.** If you don't know something, DuckDuckGo, or whatever search engine you are using will work.
2. This project is a work in progress, so bugs may occur when you compile from source. If there are any bugs you encounter, please feel free to open [a issue](https://github.com/hugopilot/CryptoCore/issues)
3. Don't be afraid to play around with the program. You will like it.

## Some defenitions
Here are some common defenitions you will find in this tutorial. It is important to know them for this tutorial:

- **CLI**. A CLI, or **Command Line Interface**, is a means of interacting with a computer program where the user (or client) issues commands to the program in the form of successive lines of text (command lines).
- **Terminal**. A terminal is a program where you can run your CLI. On Windows, you will have '**Command Prompt**' or '**PowerShell**' as your terminal and on macOS and many Linux distros your terminal is called '**Terminal**' (what a coincidence!).

## A brief introduction to cryptography
Cryptography is a collection of techniques to achive secure communication. Most of these techniques are meant to prevent a third party (like your friend, or the government) reading your message, file, etc, etc.

In the early days, before the modern age, people would write a message in such a way that it looked like nonsense, making it unreadable for any thrid party. However, you and the reciever have a special way to decode it into a normal message. This is how you would secretly message eachother.

Now in WWI the rotary encoders were invented. In WWII it was used by the germans to seretly pass messages to U-Boats. This rotary encoder was also known as the Enigma machine. The Germans thought it was unbreakable. But it wasn't.
Why? Not gonna explain now. Time issue ya know.

So modern cryptography are mainly mathmatical, and mainly executed on computers. AES or *Advanced Encryption Standard* is widly used. So is PGP (Pretty good privacy). 

## Downloading or compiling from source
*If you are lazy: you can download pre-compiled binaries [here](https://github.com/hugopilot/CryptoCore/releases)*
_______________
So now comes the fun part, and your first 'challenge' (idk if this is hard for you)

Now open your terminal and follow the instructions [here](https://github.com/hugopilot/CryptoCore/blob/master/source/Compile%20Tutorial.md
). By the time the program has enough capabilies, I will publish binaries and installers to make it easier for you to install CryptoCore.

## Using the CLI
Well after you compiled the program, start it by calling `dotnet CryptoCore.dll --help`. Here you will find a good overview of all commands availble. The basic logic here is that you start with some setup arguments, like `dotnet CryptoCore.dll --encrypt -f --AES` which will initiate a AES file encryption. After that you will follow instructions on the screen. And have a good time. So go ahead and start trying!

