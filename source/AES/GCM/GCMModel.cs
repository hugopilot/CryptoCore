﻿/* Code written by Hugo Woesthuis
 * Licensed under GPL v3
 */
namespace AES.GCM
{       
    // Key and PlainData not included, because PlainData will take up a lot of memory when stored. Key should be given directly from user/deriver.

        /// <summary>
        /// Struct container for non-sensitive encryption info
        /// </summary>
    public struct GCMEncryptionInfo
    {
        /// <summary>
        /// Authentication Data that should be included, required
        /// </summary>
        public byte[] AuthData { get; set; }

        /// <summary>
        /// Extra information that will be included with the file, but will not be encrypted. Could be used to store salts
        /// </summary>
        public byte[] NonEncPax { get; set; }
    }

    public struct GCMDecryptionInfo
    {
        /// <summary>
        /// Authentication Data to be used to verify the integity of the data
        /// </summary>
        public byte[] AuthData { get; set; }

        /// <summary>
        /// The length of the extra information.
        /// </summary>
        public int NonEncPax_length { get; set; }
    }
}
