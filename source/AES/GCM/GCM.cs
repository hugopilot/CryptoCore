﻿/* Code written by Hugo Woesthuis 
* AES-GCM like library for encrypting and decrypting data
* Security of this library still needs to be checked and confirmed
*/
using System;
using System.IO;
using System.Security.Cryptography;

namespace AES.GCM
{
    public static class GCM
    {
        // Some initalization
        private static readonly RandomNumberGenerator rn = RandomNumberGenerator.Create();

        // Encryption parameters
        // KeySize can be changed during runtime, the rest is const.
        public static int KEY_SIZE = 256;
        public const int BLOCK_SIZE = 128;

        // Generates nice random key
        public static byte[] RandomKey()
        {
            byte[] rk = new byte[KEY_SIZE / 8];
            rn.GetBytes(rk);
            return rk;
        }

        // Encryption method

            /// <summary>
            /// Encrypts data using AES GCM
            /// </summary>
            /// <param name="PLAIN">The data that should be encrypted</param>
            /// <param name="KEY">The encryption Key</param>
            /// <param name="encryptionInfo">Additional info defined in the class</param>
            /// <returns>Returns a byte[] with the encrypted data</returns>
        public static byte[] Encrypt(byte[] PLAIN, byte[] KEY, GCMEncryptionInfo encryptionInfo)
        {
            //User Error Checks
            if (KEY == null || KEY.Length != KEY_SIZE / 8)
                throw new ArgumentNullException(String.Format("Key needs to be {0} bit!", KEY_SIZE), "KEY");

            if (PLAIN == null || PLAIN.Length < 1)
                throw new ArgumentNullException("Secret Message Required!", "PLAIN");


            //Check if extra pax needs to be added
            encryptionInfo.NonEncPax = encryptionInfo.NonEncPax ?? new byte[] { };

            byte[] chipher;
            byte[] iv;

            using (var AES = new AesManaged { KeySize = KEY_SIZE, BlockSize = BLOCK_SIZE, Mode = CipherMode.CBC, Padding = PaddingMode.PKCS7})
            {

                //Create random IV
                AES.GenerateIV();
                iv = AES.IV;

                using (var encrypter = AES.CreateEncryptor(KEY, iv))
                using (var cipherStream = new MemoryStream())
                {
                    using (var cryptoStream = new CryptoStream(cipherStream, encrypter, CryptoStreamMode.Write))
                    using (var binaryWriter = new BinaryWriter(cryptoStream))
                    {
                        //Encrypt Data
                        binaryWriter.Write(PLAIN);
                    }

                    chipher = cipherStream.ToArray();
                }

            }

            //Assemble encrypted message and add authentication
            using (var hmac = new HMACSHA512(encryptionInfo.AuthData))
            using (var encryptedStream = new MemoryStream())
            {
                using (var binaryWriter = new BinaryWriter(encryptedStream))
                {
                    //Prepend salt if any
                    binaryWriter.Write(encryptionInfo.NonEncPax);
                    //Prepend IV
                    binaryWriter.Write(iv);
                    //Write Ciphertext
                    binaryWriter.Write(chipher);
                    //Clear buffer
                    binaryWriter.Flush();

                    //Authenticate all data
                    var tag = hmac.ComputeHash(encryptedStream.ToArray());
                    //Postpend tag
                    binaryWriter.Write(tag);
                }
                return encryptedStream.ToArray();
            }

        }

        //Decryption
        public static byte[] Decrypt(byte[] ÇHIPHER, byte[] KEY, GCMDecryptionInfo decryptionInfo)
        {

            //Basic Usage Error Checks
            if (KEY == null || KEY.Length != KEY_SIZE / 8)
                throw new ArgumentException(String.Format("CryptKey needs to be {0} bit!", KEY_SIZE), "cryptKey");

            if (ÇHIPHER == null || ÇHIPHER.Length == 0)
                throw new ArgumentException("Encrypted Message Required!", "encryptedMessage");

            using (var hmac = new HMACSHA512(decryptionInfo.AuthData))
            {
                var sentTag = new byte[hmac.HashSize / 8];
                //Calculate Tag
                var calcTag = hmac.ComputeHash(ÇHIPHER, 0, ÇHIPHER.Length - sentTag.Length);
                var ivLength = (BLOCK_SIZE / 8);

                //if salt length is too small just return null
                /*if (ÇHIPHER.Length < sentTag.Length + SALT_LENGTH + ivLength)
                    return null;*/

                //Grab Sent Tag
                Array.Copy(ÇHIPHER, ÇHIPHER.Length - sentTag.Length, sentTag, 0, sentTag.Length);

                //Compare Tag with constant time comparison
                var compare = 0;
                for (var i = 0; i < sentTag.Length; i++)
                    compare |= sentTag[i] ^ calcTag[i];

                //if message doesn't authenticate throw exception
                if (compare != 0)
                    throw new CryptographicException("Authentication failed! HMAC did not match.");

                using (var aes = new AesManaged
                {
                    KeySize = KEY_SIZE,
                    BlockSize = BLOCK_SIZE,
                    Mode = CipherMode.CBC,
                    Padding = PaddingMode.PKCS7
                })
                {

                    //Grab IV from message
                    var iv = new byte[ivLength];
                    Array.Copy(ÇHIPHER, decryptionInfo.NonEncPax_length, iv, 0, iv.Length);

                    using (var decrypter = aes.CreateDecryptor(KEY, iv))
                    using (var plainTextStream = new MemoryStream())
                    {
                        using (var decrypterStream = new CryptoStream(plainTextStream, decrypter, CryptoStreamMode.Write))
                        using (var binaryWriter = new BinaryWriter(decrypterStream))
                        {
                            //Decrypt Cipher Text from Message
                            binaryWriter.Write(
                              ÇHIPHER,
                              decryptionInfo.NonEncPax_length + iv.Length,
                              ÇHIPHER.Length - decryptionInfo.NonEncPax_length - iv.Length - sentTag.Length
                            );
                        }
                        //Return Plain Text
                        return plainTextStream.ToArray();
                    }
                }
            }
        }

        
    }
}
