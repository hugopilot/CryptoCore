﻿using System;
using SHAhelper;
namespace QuickSHA
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("PATH: ");
            string Path = Console.ReadLine();
            string sha = HMACSHA512.HashFile.CALCHASH_hex(Path);
            Console.WriteLine(sha);
        }
    }
}
