﻿/* SHA512 Libary that hashes strings and files with the SHA512 hashing algoritm. 
 * It can return the hashes in numerous ways.
 * 
 * Code written by Hugo Woesthuis, licensed under the GPL 3.0 license.
 */ 

using System;
using System.Text;
using System.IO;

namespace SHAhelper
{
    public static class SHA512
    {
        public static class FromString
        {
            /// <summary>
            /// Calculates the SHA512 hash of a string
            /// </summary>
            /// <param name="StringIn">The string that should be hashed</param>
            /// <returns>Returns the hash in a hex (3f) formatted string</returns>
            public static string CALCHASH_hex(string StringIn)
            {
                string hashString;
                using (var SHA512 = System.Security.Cryptography.SHA512.Create())
                {
                    var hash = SHA512.ComputeHash(Encoding.Default.GetBytes(StringIn));
                    hashString = ToHex(hash, false);
                }

                return hashString;
            }
            /// <summary>
            /// Calculates the SHA512 hash of a string
            /// </summary>
            /// <param name="StringIn">The string that should be hashed</param>
            /// <returns>Returns the hash in a hex (3F) formatted string</returns>
            public static string CALCHASH_HEX(string StringIn)
            {
                string hashString;
                using (var SHA512 = System.Security.Cryptography.SHA512.Create())
                {
                    var hash = SHA512.ComputeHash(Encoding.Default.GetBytes(StringIn));
                    hashString = ToHex(hash, true);
                }

                return hashString;
            }
            /// <summary>
            /// Calculates the SHA512 hash of a string
            /// </summary>
            /// <param name="StringIn">The string that should be hashed</param>
            /// <returns>Returns the hash in a base64 string</returns>
            public static string CALCHASH_base64(string StringIn)
            {
                string hashString;
                using (var SHA512 = System.Security.Cryptography.SHA512.Create())
                {
                    var hash = SHA512.ComputeHash(Encoding.Default.GetBytes(StringIn));
                    hashString = Convert.ToBase64String(hash);
                }

                return hashString;
            }
        }
        public static class HashFile
        {
            /// <summary>
            /// Calculates the SHA512 hash of a file.
            /// </summary>
            /// <param name="Path">The file path to the file that should be hashed</param>
            /// <returns>Returns the hash in a hex (3f) formatted string</returns>
            public static string CALCHASH_hex(string Path)
            {
                string hashString;
                byte[] BytesFromFile = File.ReadAllBytes(Path);
                using (var SHA512 = System.Security.Cryptography.SHA512.Create())
                {
                    var hash = SHA512.ComputeHash(BytesFromFile);
                    hashString = ToHex(hash, false);
                }

                return hashString;
            }
            /// <summary>
            /// Calculates the SHA512 hash of a file.
            /// </summary>
            /// <param name="Path">The file path to the file that should be hashed</param>
            /// <returns>Returns the hash in a hex (3F) formatted string</returns>
            public static string CALCHASH_HEX(string Path)
            {
                string hashString;
                byte[] BytesFromFile = File.ReadAllBytes(Path);
                using (var SHA512 = System.Security.Cryptography.SHA512.Create())
                {
                    var hash = SHA512.ComputeHash(BytesFromFile);
                    hashString = ToHex(hash, true);
                }

                return hashString;
            }
            /// <summary>
            /// Calculates the SHA512 hash of a file.
            /// </summary>
            /// <param name="Path">The file path to the file that should be hashed</param>
            /// <returns>Returns the hash in a base64 formatted string</returns>
            public static string CALCHASH_base64(string Path)
            {
                string hashString;
                byte[] BytesFromFile = File.ReadAllBytes(Path);
                using (var SHA512 = System.Security.Cryptography.SHA512.Create())
                {
                    var hash = SHA512.ComputeHash(BytesFromFile);
                    hashString = Convert.ToBase64String(hash);
                }

                return hashString;
            }
        }
        private static string ToHex(byte[] bytes, bool upperCase)
        {
            StringBuilder result = new StringBuilder(bytes.Length * 2);
            for (int i = 0; i < bytes.Length; i++)
                result.Append(bytes[i].ToString(upperCase ? "X2" : "x2"));
            return result.ToString();
        }
    }
}
