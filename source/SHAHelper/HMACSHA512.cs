﻿using System;
using System.IO;
using System.Text;

namespace SHAhelper
{
    public static class HMACSHA512
    {
        public static class FromString
        {
            public static string CALCHASH_hex(string StringIn)
            {
                string hashString;
                using (var HMACSHA512 = System.Security.Cryptography.HMACSHA512.Create())
                {
                    var hash = HMACSHA512.ComputeHash(Encoding.Default.GetBytes(StringIn));
                    hashString = ToHex(hash, false);
                }

                return hashString;
            }
            public static string CALCHASH_HEX(string StringIn)
            {
                string hashString;
                using (var HMACSHA512 = System.Security.Cryptography.HMACSHA512.Create())
                {
                    var hash = HMACSHA512.ComputeHash(Encoding.Default.GetBytes(StringIn));
                    hashString = ToHex(hash, true);
                }

                return hashString;
            }
            public static string CALCHASH_base64(string StringIn)
            {
                string hashString;
                using (var HMACSHA512 = System.Security.Cryptography.HMACSHA512.Create())
                {
                    var hash = HMACSHA512.ComputeHash(Encoding.Default.GetBytes(StringIn));
                    hashString = Convert.ToBase64String(hash);
                }

                return hashString;
            }
        }
        public static class HashFile
        {
            public static string CALCHASH_hex(string Path)
            {
                string hashString;
                byte[] BytesFromFile = File.ReadAllBytes(Path);
                using (var HMACSHA512 = System.Security.Cryptography.HMACSHA512.Create())
                {
                    var hash = HMACSHA512.ComputeHash(BytesFromFile);
                    hashString = ToHex(hash, false);
                }

                return hashString;
            }
            public static string CALCHASH_HEX(string Path)
            {
                string hashString;
                byte[] BytesFromFile = File.ReadAllBytes(Path);
                using (var HMACSHA512 = System.Security.Cryptography.HMACSHA512.Create())
                {
                    var hash = HMACSHA512.ComputeHash(BytesFromFile);
                    hashString = ToHex(hash, true);
                }

                return hashString;
            }
            public static string CALCHASH_base64(string Path)
            {
                string hashString;
                byte[] BytesFromFile = File.ReadAllBytes(Path);
                using (var HMACSHA512 = System.Security.Cryptography.HMACSHA512.Create())
                {
                    var hash = HMACSHA512.ComputeHash(BytesFromFile);
                    hashString = Convert.ToBase64String(hash);
                }

                return hashString;
            }
        }
        private static string ToHex(byte[] bytes, bool upperCase)
        {
            StringBuilder result = new StringBuilder(bytes.Length * 2);
            for (int i = 0; i < bytes.Length; i++)
                result.Append(bytes[i].ToString(upperCase ? "X2" : "x2"));
            return result.ToString();
        }

    }
}

