﻿using System;
using System.Text;
using System.IO;

namespace SHAhelper
{
    public static class SHA1
    {
        public static class FromString
        {
            public static string CALCHASH_hex(string StringIn)
            {
                string hashed;
                using(var SHA1 = System.Security.Cryptography.SHA1.Create())
                {
                    var hash = SHA1.ComputeHash(Encoding.Default.GetBytes(StringIn));
                    hashed = ToHex(hash, false);
                }
                return hashed;
            }
            public static string CALCHASH_HEX(string StringIn)
            {
                string hashed;
                using (var SHA1 = System.Security.Cryptography.SHA1.Create())
                {
                    var hash = SHA1.ComputeHash(Encoding.Default.GetBytes(StringIn));
                    hashed = ToHex(hash, true);
                }
                return hashed;
            }
            public static string CALCHASH_base64(string StringIn)
            {
                string hashed;
                using (var SHA1 = System.Security.Cryptography.SHA1.Create())
                {
                    var hash = SHA1.ComputeHash(Encoding.Default.GetBytes(StringIn));
                    hashed = Convert.ToBase64String(hash);
                }
                return hashed;
            }
        }
        public static class HashFile
        {
            public static string CALCHASH_hex(string Path)
            {
                string hashed;
                using (var SHA1 = System.Security.Cryptography.SHA1.Create())
                {
                    var hash = SHA1.ComputeHash(File.ReadAllBytes(Path));
                    hashed = ToHex(hash, false);
                }
                return hashed;
            }
            public static string CALCHASH_HEX(string Path)
            {
                string hashed;
                using (var SHA1 = System.Security.Cryptography.SHA1.Create())
                {
                    var hash = SHA1.ComputeHash(File.ReadAllBytes(Path));
                    hashed = ToHex(hash, true);
                }
                return hashed;
            }
            public static string CALCHASH_base64(string Path)
            {
                string hashed;
                using (var SHA1 = System.Security.Cryptography.SHA1.Create())
                {
                    var hash = SHA1.ComputeHash(File.ReadAllBytes(Path));
                    hashed = Convert.ToBase64String(hash);
                }
                return hashed;
            }
        }
        private static string ToHex(byte[] bytes, bool upperCase)
        {
            StringBuilder result = new StringBuilder(bytes.Length * 2);
            for (int i = 0; i < bytes.Length; i++)
                result.Append(bytes[i].ToString(upperCase ? "X2" : "x2"));
            return result.ToString();
        }
    }
}
