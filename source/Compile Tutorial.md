# Compiling from source
A full tutorial how to compile from source, and run the program.

1. Download the [latest .NET Core SDK from here](https://github.com/dotnet/core/tree/master/release-notes/)
2. Clone the repository.

We do that by going into Command Prompt (CMD, Windows), PowerShell (PS, Windows) or a Shell (Unix/Linux based) and call `git clone https://github.com/hugopilot/CryptoCore.git` 

OR 

Go to https://github.com/hugopilot/CryptoCore and download the master branch as a zip and extract the zip somewhere you like.

3. After downloading the repository, go to the main folder (`~\CryptoCore`)

4. Restore the dependencies by calling `dotnet restore` in your terminal.


5. Build the project by calling `dotnet build` in your terminal.

_NOTE: In some OS'ses, you will need to run the `dotnet build` and `dotnet restore` command in Administrator/sudo mode to avoid errors_


Your output should be something like this:
```
PS C:\Users\hugow\source\repos\CryptoCore\source\CryptoCore> dotnet build
Microsoft (R) Build Engine version 15.6.82.30579 for .NET Core
Copyright (C) Microsoft Corporation. All rights reserved.

  Restore completed in 44,7 ms for C:\Users\hugow\source\repos\CryptoCore\source\CryptoCore\CryptoCore.csproj.
  Restore completed in 44,7 ms for C:\Users\hugow\source\repos\CryptoCore\source\SecureIO\SecureIO.csproj.
  Restore completed in 44,69 ms for C:\Users\hugow\source\repos\CryptoCore\source\AES\AES.csproj.
  Restore completed in 44,69 ms for C:\Users\hugow\source\repos\CryptoCore\source\DeriveBytes\RFC2898.csproj.
  Restore completed in 44,69 ms for C:\Users\hugow\source\repos\CryptoCore\source\SHAHelper\SHAhelper.csproj.
  SHAhelper -> C:\Users\hugow\source\repos\CryptoCore\source\SHAHelper\bin\Debug\netcoreapp2.0\SHAhelper.dll
  SecureIO -> C:\Users\hugow\source\repos\CryptoCore\source\SecureIO\bin\Debug\netcoreapp2.0\SecureIO.dll
  AES -> C:\Users\hugow\source\repos\CryptoCore\source\AES\bin\Debug\netcoreapp2.0\AES.dll
  RFC2898 -> C:\Users\hugow\source\repos\CryptoCore\source\DeriveBytes\bin\Debug\netcoreapp2.0\RFC2898.dll
  CryptoCore -> C:\Users\hugow\source\repos\CryptoCore\source\CryptoCore\bin\Debug\netcoreapp2.0\CryptoCore.dll

Build succeeded.
    0 Warning(s)
    0 Error(s)

Time Elapsed 00:00:04.28
```
6. Go to `~\CryptoCore\source\CryptoCore` and copy the LIBRAIES.xml file to `~\CryptoCore\source\CrpytoCore\bin\Debug\netcoreapp2.0`
  
7. Navigate in CMD, PS or Shell to the Debug folder, which is located in `~\CryptoCore\source\CrpytoCore\bin\Debug\netcoreapp2.0` (Win) or `~/CryptoCore/source/CrpytoCore/bin/Debug/netcoreapp2.0` (Unix)
8. Call `dotnet CryptoCore.dll` in CMD, PS or Shell to run the program.

**NOTE:** To start CryptoCore you need to pass some arguments, for example `dotnet CryptoCore.dll --encrypt -f --AES`! 

