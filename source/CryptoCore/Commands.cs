﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using CryptoCore.CommandHandlers;

namespace CryptoCore
{
    class Commands
    {
        /// <summary>
        /// Execute a command though command arguments
        /// </summary>
        /// <param name="args">Arguments that need to be passed</param>
        public static void ExecCommand(string[] args)
        {
            string mainCommand = args[0];
            switch (mainCommand)
            {
                case "--help":
                    PrintIntro();
                    PrintHelp();
                    break;


                case "--encrypt":
                    string FileorString = args[1];

                    // Boolean that states if a file should be encrypted
                    bool FILE = true;

                    // Set the FILE boolean or throw an exception if input is invalid
                    if (FileorString == "-f" || FileorString == "--file"){ FILE = true; }
                    else if(FileorString == "-s" || FileorString == "--string") { FILE = false; }
                    else if(FileorString == "-?" || FileorString == "--help"){ Console.WriteLine(); Console.WriteLine("Usage: --encrypt [-f | -s] [--method] [String to Encrypt | InputFile] [OutputFile]"); Console.WriteLine(); Console.WriteLine("Example: --encrypt -f --AES media/lol.mp4 OR --encrypt -s --AES 'A string to encrypt'"); Console.WriteLine(); }
                    else { throw new ArgumentOutOfRangeException("FileorString", FileorString, "Not a correct argument, use -s or --string for encrypting a string, use -f or --file for encrypting files."); }

                    // Get desired encryption method or throw exception of input in invalid
                    string method = args[2];
                    switch (method)
                    {
                        case "--AES": CommandHandlers.AESCommand.EncryptionFollowup(FILE, args); break;
                        case "--PGP": /*TODO: PGP Encryption*/ break;
                        default: throw new ArgumentOutOfRangeException("method", method, "Not a valid encryption method");
                    }

                    break;


                case "--decrypt":
                    string FileoRString = args[1];

                    // Boolean that states if a file should be decrypted
                    bool FILe;

                    // Set the FILE boolean or throw an exception if input is invalid
                    if (FileoRString == "-f" || FileoRString == "--file") { FILe = true; }
                    else if (FileoRString == "-s" || FileoRString == "--string") { FILe = false; }
                    else { throw new ArgumentOutOfRangeException("FileorString", FileoRString, "Not a correct argument, use -s or --string for encrypting a string, use -f or --file for encrypting files."); }
                    string Method = args[2];
                    switch (Method)
                    {
                        case "--AES": CommandHandlers.AESCommand.DecryptionFollowup(FILe, args); break;
                        case "--PGP": /*TODO: PGP Decryption*/ break;
                        default: throw new ArgumentOutOfRangeException("method", Method, "Not a valid encryption method");
                    }
                    break;


                case "--wipe":
                    switch (args[1])
                    {
                        case "--help":
                        case "-?":
                            Console.WriteLine("\n");
                            Console.WriteLine("Usage: --wipe [-f|-d|-D] [mountpoint|paths] [-t|-n] [number]");
                            Console.WriteLine();
                            Console.WriteLine("-f: wipes a file");
                            Console.WriteLine("-d: Wipes available disk space of a drive");
                            Console.WriteLine("-D: Wipes a drive (-n nulls the drive aswell)");
                            Console.WriteLine();
                            Console.WriteLine("Example: --wipe -d F: -t 3");
                            Console.WriteLine("Example: --wipe -f '/home/auser/Documents/afile.txt' '/media/auser/A Drive/wee.txt''");
                            Console.WriteLine();
                            break;

                        case "-f":
                            Wiping.WipeFileFollowup(args);
                            break;
                        case "-d":
                            Wiping.WipeAvailableSpace(args);
                            break;
                        case "-D":
                            Wiping.WipeDisk(args);
                            break;
                        default:
                            throw new Exception("Bad argument");

                    }
                    break;


                case "--verify":
                    if(args[1] == "-?" || args[1] == "--help")
                    {
                        Console.WriteLine("\n");
                        Console.WriteLine("Usage: --verify -m [method] [-f|--file|-s|--string] [string|path to file] -h [hash]");
                        Console.WriteLine();
                        Console.WriteLine("Example: --verify -m SHA256 -s 'A string' -h 'f4c4439c6269dd5853d324d45d9256b34973f1ecfed07ce62c2f0171a8d20a75'");
                        Console.WriteLine();
                        return;
                    }
                    VerificationFollow.VerificationFollowUp(args);
                    break;


                case "--genhash":
                case "--generatehash":
                    if (args[1] == "-?" || args[1] == "--help")
                    {
                        Console.WriteLine("\n");
                        Console.WriteLine("Usage: --genhash -m [method] [-f|--file|-s|--string] [File path|String to hash] -F [base64|hex|HEX]");
                        Console.WriteLine();
                        Console.WriteLine("Example: --genhash -m SHA512 -f 'C:\foo.txt' -F hex");
                        Console.WriteLine("Example: --genhash -m SHA256 -s 'Kiekeboe!' -F base64");
                        Console.WriteLine();
                        return;
                    }
                    gh.Generate(args);
                    break;
                default:
                    Program.ColoredConsoleWrite(ConsoleColor.Red, mainCommand + " is not a valid command, please try again with a different command.");
                    break;
                
            }
        }
        /// <summary>
        /// Execute command from a XML file
        /// </summary>
        /// <param name="path">Path to the XML file</param>
        public static void ExecFromXML(string path)
        {
            XmlNodeList command = XMLGetElementArray(path, "command");
            string cmd = command[0].InnerText;
            switch (cmd)
            {
                case "encrypt":
                    //Do something
                    break;
                default:
                    throw new XmlException("Bad command in <command>");
            }

        }
        private static XmlNodeList XMLGetElementArray(string path, string TargetElement)
        {
            // XmlDoc initalization
            var xml = new XmlDocument();

            // Load XML file from patch
            xml.Load(path);

            // Get elements
            XmlNodeList element = xml.GetElementsByTagName(TargetElement);

            return element;

        }
        private static void PrintIntro()
        {
            Console.ForegroundColor = ConsoleColor.White;
            string[] lines = new string[] {string.Empty, "Welcome to CryptoCore!",
                "----------------------------",
                "For a list of commands, call the --help argument",
                string.Empty,
                "This program is licensed under the GNU General Public License v3.0.",
                "For more information, visit https://www.gnu.org/licenses/gpl.html or read the LICENSE file in the repository",
                "----------------------------", string.Empty};
            foreach (string line in lines)
            {
                Console.WriteLine(line);
            }
        }
        private static void PrintHelp()
        {
            Console.ForegroundColor = ConsoleColor.White;
            string[] lines = new string[] { string.Empty, "All commands:", string.Empty, "help: Spams this", "encrypt: Encrypt a file/string", "decrypt: Decrypt a file/string", "verify: Verify the intergety of a file", "wipe: For securly wiping files/drives", string.Empty };
            foreach(string line in lines) { Console.WriteLine(line); }
        }
    }
}
