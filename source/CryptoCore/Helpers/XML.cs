﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace CryptoCore.Helpers
{
    class XML
    {
        public static XmlNodeList XMLGetElementArray(string path, string TargetElement)
        {
            // XmlDoc initalization
            var xml = new XmlDocument();

            // Load XML file from patch
            xml.Load(path);

            // Get elements
            XmlNodeList element = xml.GetElementsByTagName(TargetElement);

            return element;

        }
    }
}
