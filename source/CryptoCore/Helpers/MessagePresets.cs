﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CryptoCore.Helpers
{
    public static class MessagePresets
    {
        public static void WriteInfo(string Message)
        {
            Program.ColoredConsoleWrite(ConsoleColor.Cyan, "INFO: ");
            Program.ColoredConsoleWrite(ConsoleColor.White, Message);
        }
        public static void WriteError(string Message)
        {
            Program.ColoredConsoleWrite(ConsoleColor.Red, "ERROR: " + Message);
        }
        public static void WriteWarning(string Message)
        {
            Program.ColoredConsoleWrite(ConsoleColor.Yellow, "WARNING: " + Message);
        }
        public static void WriteInput(string Message)
        {
            Program.ColoredConsoleWrite(ConsoleColor.Magenta, "INPUT: ");
            Program.ColoredConsoleWrite(ConsoleColor.White, Message);
        }
        public static void WriteSuccess(string Message)
        {
            Program.ColoredConsoleWrite(ConsoleColor.Green, "SUCCESS: ");
            Program.ColoredConsoleWrite(ConsoleColor.White, Message);
            
        }
        public static void WriteOK()
        {
            Program.ColoredConsoleWrite(ConsoleColor.Green, "OK");
        }
    }
}
