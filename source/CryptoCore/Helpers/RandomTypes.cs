﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace CryptoCore.Helpers
{
    public class RandomTypes
    {
        private static Random r = new Random();
        public static string RandomString(int length)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Range(1, length).Select(_ => chars[r.Next(chars.Length)]).ToArray());
        }
    }
}
