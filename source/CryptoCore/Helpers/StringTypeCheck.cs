﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CryptoCore.Helpers
{
    public static class StringTypeCheck
    {
        public static bool IsBase64(string base64String)
        {
            // Credit: oybek https://stackoverflow.com/users/794764/oybek
            if (base64String == null || base64String.Length == 0 || base64String.Length % 4 != 0
               || base64String.Contains(" ") || base64String.Contains("\t") || base64String.Contains("\r") || base64String.Contains("\n") || !base64String.Contains("="))
                return false;

            try
            {
                Convert.FromBase64String(base64String);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool IsHex(string chars)
        {
            bool isHex;
            foreach (char c in chars)
            {
                isHex = ((c >= '0' && c <= '9') ||
                         (c >= 'a' && c <= 'f'));

                if (!isHex)
                    return false;
            }
            return true;
        }
        public static bool IsHEX(string chars)
        {
            bool isHex;
            foreach (char c in chars)
            {
                isHex = ((c >= '0' && c <= '9') ||
                         (c >= 'A' && c <= 'F'));

                if (!isHex)
                    return false;
            }
            return true;
        }
    }
}
