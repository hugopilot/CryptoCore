﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Data.Sqlite;
using System.IO;
using System.Xml;
using SHAhelper;
using CryptoCore.Helpers;
using CryptoCore.DbHandlers;
using CryptoCore.DbHandlers.Commands;

namespace CryptoCore
{
    class CheckDependencies
    {
        /// <summary>
        /// Checks if all external libaries and dependencies are present in their folders
        /// </summary>
        
        public static string RootDir = AppDomain.CurrentDomain.BaseDirectory;
        public static void CheckFilePresence()
        {
            XmlNodeList xml = XMLGetElementArray(RootDir + "LIBARIES.xml", "FileName");
            for(int i = 0; i < xml.Count; i++)
            {
                string fn = xml[i].InnerText;
                if(!File.Exists(RootDir + fn)) { throw new IOException(fn + " doesn't exist. Please check that the file is present in the root folder. Continuing might result in limited use capability."); }
            }
            
        }
        /// <summary>
        /// Checks the SHA checksum of the libaries.
        /// </summary>
        public static void CheckFileValid()
        {
            //Get nodelist from both filenames and checksums
            XmlNodeList xmlfn = XMLGetElementArray(RootDir + "LIBARIES.xml", "FileName");
            XmlNodeList xmlchecsum = XMLGetElementArray(RootDir + "LIBARIES.xml", "SHA512");

            // Check if both have the same count, otherwise exception will be thrown
            if(xmlfn.Count == xmlchecsum.Count)
            {
                for(int i = 0; i < xmlchecsum.Count; i++)
                {
                    string fn = xmlfn[i].InnerText;
                    string cs = xmlchecsum[i].InnerText;
                    string Acs = HMACSHA512.HashFile.CALCHASH_hex(RootDir + fn);
                    if(Acs != cs) { throw new Exception(fn + " doesn't have the same checksum as described in LIBARIES.xml. It might be corrupted or compromised. Continuing might add a security risk and/or limited use capability."); }
                }
            }
            else if(xmlfn.Count != xmlchecsum.Count)
            {
                throw new XmlException("Can't start validation! Parameters are missing.");
            }
        }
        /// <summary>
        /// Gets a XMLNodeList from a XML file by searching for a specific element
        /// </summary>
        /// <param name="path">File path to target XML file</param>
        /// <param name="TargetElement">The element to search for in the xml file</param>
        /// <returns>Returns a XMLNodeList with all search results</returns>
        private static XmlNodeList XMLGetElementArray(string path, string TargetElement)
        {
            // XmlDoc initalization
            var xml = new XmlDocument();

            // Load XML file from patch
            xml.Load(path);

            // Get elements
            XmlNodeList element = xml.GetElementsByTagName(TargetElement);

            return element;
           
        }
        public static void CheckForDatabasePresence()
        {
            XmlNodeList xmlp = XMLGetElementArray(RootDir + "LIBARIES.xml", "Path");
            string path = xmlp[0].InnerText;
            DbInfo info = new DbInfo();
            info.DbPath = path;
            info.HasPassword = false;
            if (!File.Exists(path))
            {
                info = Connection.AddConnectionAndInfo(info);
                InitalizeDatabase.Initalize(info);
            }
            
            
        }
    }
}
