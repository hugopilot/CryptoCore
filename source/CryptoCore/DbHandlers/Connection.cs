﻿/* Code written by Hugo Woesthuis
 * Licensed under GPL-3.0
 * 
 * Library for enstablishing a database connection
 */

using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Data.Sqlite;

namespace CryptoCore.DbHandlers
{
    public static class Connection
    {
        public static DbInfo AddConnectionAndInfo(DbInfo dbInfo)
        {
            // Check if a connection string is present
            //      If not, create one
            if(dbInfo.DbConnectionString == null)
            {
                dbInfo.DbConnectionString = ConstructConnectionString(dbInfo);
            }
            
            // Initalize a SQLite database connection
            SqliteConnection con = new SqliteConnection();
            con.ConnectionString = dbInfo.DbConnectionString;
            dbInfo.Connection = con;

            return dbInfo;

        }

        private static string ConstructConnectionString(DbInfo ConnectionInfo)
        {
            if (ConnectionInfo.DbPath == null) { throw new ArgumentNullException("DbPath can not be null"); }

            return "Data Source=" + ConnectionInfo.DbPath + ";";
        }
    }
}
