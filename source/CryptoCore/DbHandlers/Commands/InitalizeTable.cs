﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Microsoft.Data.Sqlite;

namespace CryptoCore.DbHandlers.Commands
{
    public static class InitalizeDatabase
    {
        public static void Initalize(DbInfo dbInfo)
        {
            CreateKeyDataTable(dbInfo);
        }
        private static void CreateKeyDataTable(DbInfo dbInfo)
        {
            ExecuteCommand.Execute("CREATE TABLE KeyData (DecKey varchar(16), DHash varchar(256));", dbInfo, null, null);
        }
    }
}
