﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Data.Sqlite;

namespace CryptoCore.DbHandlers.Commands
{
    public class Data
    {
        public static void Add(string Key, string Hash, DbInfo dbInfo)
        {
            string[] parameters = new string[] { Key, Hash };
            SqliteType[] types = new SqliteType[] { SqliteType.Text, SqliteType.Text };
            ExecuteCommand.Execute("INSERT INTO KeyData VALUES (@0, @1)", dbInfo, parameters, types);
        }
    }
}
