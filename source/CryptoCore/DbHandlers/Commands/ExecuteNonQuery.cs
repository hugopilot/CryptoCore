﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Data.Sqlite;


namespace CryptoCore.DbHandlers.Commands
{
    public class ExecuteCommand
    {
        public static void Execute(string SqlCommand, DbInfo dbInfo, string[] parameterValues, SqliteType[] parameterTypes)
        {
            SqliteConnection con = dbInfo.Connection;
            SqliteCommand cmd = new SqliteCommand();
            cmd.Connection = con;
            con.Open();
            
            if(parameterValues.Length != parameterTypes.Length) { throw new OverflowException("The parameter values don't have the same length as the types"); }
            for(int i = 0; i < parameterTypes.Length; i++)
            {
                cmd.Parameters.Add("@" + i, parameterTypes[i], 256).Value = parameterValues[i];
            }
            cmd.CommandText = SqlCommand;
            cmd.ExecuteNonQuery();
            con.Close();
            con.Dispose();
        }
    }
}
