﻿using Microsoft.Data.Sqlite;

namespace CryptoCore.DbHandlers
{
    public struct DbInfo
    {
        public string DbPath { get; set; }
        public bool HasPassword { get; set; }
        public string DbPassword { get; set; }
        public string DbConnectionString { get; set; }
        public SqliteConnection Connection { get; set; }
    }
}
