﻿/* Main CLI program.
 * Use ColoredConsoleWrite() instead of Console.Write()
 * See color standards in above ColoredConsoleWrite()
 * 
 * Code written by Hugo Woesthuis
 * Licensed with the GNU GPL 3.0 license
 */

using System;
using System.Security;
using System.Text;
using RFC2898;
using AES;
using System.IO;
namespace CryptoCore
{
    class Program
    {
        static void Main(string[] args)
        {
            Setup();
            try
            {
                //Check if a file should be used to pass settings
                if (File.Exists(args[0]))
                {

                }
                else
                {
                    Commands.ExecCommand(args);
                }
            }
            catch(Exception e)
            {
                ColoredConsoleWrite(ConsoleColor.Red, "ERROR: " + e.Message+"\n");
            }
            ColoredConsoleWrite(ConsoleColor.White, "END of program, exiting. \n");
        }



        static void Setup()
        {
            ColoredConsoleWrite(ConsoleColor.Cyan, "INFO: ");
            ColoredConsoleWrite(ConsoleColor.White, "Starting dependency check...");
            Console.WriteLine();
            try
            {
                ColoredConsoleWrite(ConsoleColor.Cyan, "INFO: ");
                ColoredConsoleWrite(ConsoleColor.White, "Checking file presence...");
                CheckDependencies.CheckFilePresence();
                ColoredConsoleWrite(ConsoleColor.Green, "OK ");
                Console.WriteLine();
                ColoredConsoleWrite(ConsoleColor.Cyan, "INFO: ");
                ColoredConsoleWrite(ConsoleColor.White, "Checking Key Database...");
                CheckDependencies.CheckForDatabasePresence();
                ColoredConsoleWrite(ConsoleColor.Green, "OK");
                Console.WriteLine();
            }
            catch(Exception e)
            {
                ColoredConsoleWrite(ConsoleColor.Yellow, "WARNING: " + e.Message);
                Console.WriteLine();
            }
            Console.WriteLine();
        }


        /* Color standards:
         * Cyan: To be used for "INFO: " or any other kind of informative prefix
         * Red: To be used with error messages
         * Yellow: For warning messages
         * White: Normal text
         * Green: To be used for "OK: ", "SUCCESS: ", "COMPLETED: ", or someother kind of success prefix.
         * Magnenta: For stating inputs
         */
        public static void ColoredConsoleWrite(ConsoleColor color, string text)
        {
            ConsoleColor originalColor = Console.ForegroundColor;
            Console.ForegroundColor = color;
            Console.Write(text);
            Console.ForegroundColor = originalColor;
        }
        public static string ReadPassword()
        {
            string password = "";
            ConsoleKeyInfo info = Console.ReadKey(true);
            while (info.Key != ConsoleKey.Enter)
            {
                if (info.Key != ConsoleKey.Backspace)
                {
                    Console.Write("*");
                    password += info.KeyChar;
                }
                else if (info.Key == ConsoleKey.Backspace)
                {
                    if (!string.IsNullOrEmpty(password))
                    {
                        // remove one character from the list of password characters
                        password = password.Substring(0, password.Length - 1);
                        // get the location of the cursor
                        int pos = Console.CursorLeft;
                        // move the cursor to the left by one character
                        Console.SetCursorPosition(pos - 1, Console.CursorTop);
                        // replace it with space
                        Console.Write(" ");
                        // move the cursor to the left by one character again
                        Console.SetCursorPosition(pos - 1, Console.CursorTop);
                    }
                }
                info = Console.ReadKey(true);
            }
            // add a new line because user pressed enter at the end of their password
            Console.WriteLine();
            return password;
        }
    }
}
