﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CryptoCore.CommandHandlers.GenerateHash
{
    public struct HashInfo
    {
        /// <summary>
        /// String to hash, or a path to a file that should be hashed
        /// </summary>
        public string InputString { get; set; }

        /// <summary>
        /// Boolean that states if a file should be hashed or a string
        /// </summary>
        public bool GenerateForFile { get; set; }
    }

    /// <summary>
    /// The desired method to use for hashing
    /// </summary>
    public enum HashMethod
    {
        SHA1,
        SHA256,
        SHA384,
        SHA512,
        HMACSHA1,
        HMACSHA256,
        HMACSHA384,
        HMACSHA512
    }
    public enum FormatOptions
    {
        hex,
        HEX,
        base64
    }
}
