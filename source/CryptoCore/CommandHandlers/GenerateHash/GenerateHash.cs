﻿using System;
using System.Collections.Generic;
using System.Text;
using SHAhelper;

namespace CryptoCore.CommandHandlers.GenerateHash
{
    public class GenerateHash
    {
        public static string Generate(HashInfo hashInfo, HashMethod method, FormatOptions format)
        {
            if(hashInfo.InputString == null) { throw new ArgumentNullException("There is no infomation in InputString!"); }
            switch (format)
            {
                case FormatOptions.hex:
                    switch (method)
                    {
                        case HashMethod.SHA1:
                            if (hashInfo.GenerateForFile)
                            {
                                return SHA1.HashFile.CALCHASH_hex(hashInfo.InputString);
                            }
                            else if (!hashInfo.GenerateForFile)
                            {
                                return SHA1.FromString.CALCHASH_hex(hashInfo.InputString);
                            }
                            break;
                        case HashMethod.SHA256:
                            if (hashInfo.GenerateForFile)
                            {
                                return SHA256.HashFile.CALCHASH_hex(hashInfo.InputString);
                            }
                            else if (!hashInfo.GenerateForFile)
                            {
                                return SHA256.FromString.CALCHASH_hex(hashInfo.InputString);
                            }
                            break;
                        case HashMethod.SHA384:
                            if (hashInfo.GenerateForFile)
                            {
                                return SHA384.HashFile.CALCHASH_hex(hashInfo.InputString);
                            }
                            else if (!hashInfo.GenerateForFile)
                            {
                                return SHA384.FromString.CALCHASH_hex(hashInfo.InputString);
                            }
                            break;
                        case HashMethod.SHA512:
                            if (hashInfo.GenerateForFile)
                            {
                                return SHA512.HashFile.CALCHASH_hex(hashInfo.InputString);
                            }
                            else if (!hashInfo.GenerateForFile)
                            {
                                return SHA512.FromString.CALCHASH_hex(hashInfo.InputString);
                            }
                            break;
                        case HashMethod.HMACSHA1:
                            if (hashInfo.GenerateForFile)
                            {
                                return HMACSHA1.HashFile.CALCHASH_hex(hashInfo.InputString);
                            }
                            else if (!hashInfo.GenerateForFile)
                            {
                                return HMACSHA1.FromString.CALCHASH_hex(hashInfo.InputString);
                            }
                            break;
                        case HashMethod.HMACSHA256:
                            if (hashInfo.GenerateForFile)
                            {
                                return HMACSHA256.HashFile.CALCHASH_hex(hashInfo.InputString);
                            }
                            else if (!hashInfo.GenerateForFile)
                            {
                                return HMACSHA256.FromString.CALCHASH_hex(hashInfo.InputString);
                            }
                            break;
                        case HashMethod.HMACSHA384:
                            if (hashInfo.GenerateForFile)
                            {
                                return HMACSHA384.HashFile.CALCHASH_hex(hashInfo.InputString);
                            }
                            else if (!hashInfo.GenerateForFile)
                            {
                                return HMACSHA384.FromString.CALCHASH_hex(hashInfo.InputString);
                            }
                            break;
                        case HashMethod.HMACSHA512:
                            if (hashInfo.GenerateForFile)
                            {
                                return HMACSHA512.HashFile.CALCHASH_hex(hashInfo.InputString);
                            }
                            else if (!hashInfo.GenerateForFile)
                            {
                                return HMACSHA512.FromString.CALCHASH_hex(hashInfo.InputString);
                            }
                            break;
                    }
                    break;
                case FormatOptions.HEX:
                    switch (method)
                    {
                        case HashMethod.SHA1:
                            if (hashInfo.GenerateForFile)
                            {
                                return SHA1.HashFile.CALCHASH_HEX(hashInfo.InputString);
                            }
                            else if (!hashInfo.GenerateForFile)
                            {
                                return SHA1.FromString.CALCHASH_HEX(hashInfo.InputString);
                            }
                            break;
                        case HashMethod.SHA256:
                            if (hashInfo.GenerateForFile)
                            {
                                return SHA256.HashFile.CALCHASH_HEX(hashInfo.InputString);
                            }
                            else if (!hashInfo.GenerateForFile)
                            {
                                return SHA256.FromString.CALCHASH_HEX(hashInfo.InputString);
                            }
                            break;
                        case HashMethod.SHA384:
                            if (hashInfo.GenerateForFile)
                            {
                                return SHA384.HashFile.CALCHASH_HEX(hashInfo.InputString);
                            }
                            else if (!hashInfo.GenerateForFile)
                            {
                                return SHA384.FromString.CALCHASH_HEX(hashInfo.InputString);
                            }
                            break;
                        case HashMethod.SHA512:
                            if (hashInfo.GenerateForFile)
                            {
                                return SHA512.HashFile.CALCHASH_HEX(hashInfo.InputString);
                            }
                            else if (!hashInfo.GenerateForFile)
                            {
                                return SHA512.FromString.CALCHASH_HEX(hashInfo.InputString);
                            }
                            break;
                        case HashMethod.HMACSHA1:
                            if (hashInfo.GenerateForFile)
                            {
                                return HMACSHA1.HashFile.CALCHASH_HEX(hashInfo.InputString);
                            }
                            else if (!hashInfo.GenerateForFile)
                            {
                                return HMACSHA1.FromString.CALCHASH_HEX(hashInfo.InputString);
                            }
                            break;
                        case HashMethod.HMACSHA256:
                            if (hashInfo.GenerateForFile)
                            {
                                return HMACSHA256.HashFile.CALCHASH_HEX(hashInfo.InputString);
                            }
                            else if (!hashInfo.GenerateForFile)
                            {
                                return HMACSHA256.FromString.CALCHASH_HEX(hashInfo.InputString);
                            }
                            break;
                        case HashMethod.HMACSHA384:
                            if (hashInfo.GenerateForFile)
                            {
                                return HMACSHA384.HashFile.CALCHASH_HEX(hashInfo.InputString);
                            }
                            else if (!hashInfo.GenerateForFile)
                            {
                                return HMACSHA384.FromString.CALCHASH_HEX(hashInfo.InputString);
                            }
                            break;
                        case HashMethod.HMACSHA512:
                            if (hashInfo.GenerateForFile)
                            {
                                return HMACSHA512.HashFile.CALCHASH_HEX(hashInfo.InputString);
                            }
                            else if (!hashInfo.GenerateForFile)
                            {
                                return HMACSHA512.FromString.CALCHASH_HEX(hashInfo.InputString);
                            }
                            break;
                    }
                    break;
                case FormatOptions.base64:
                    switch (method)
                    {
                        case HashMethod.SHA1:
                            if (hashInfo.GenerateForFile)
                            {
                                return SHA1.HashFile.CALCHASH_base64(hashInfo.InputString);
                            }
                            else if (!hashInfo.GenerateForFile)
                            {
                                return SHA1.FromString.CALCHASH_base64(hashInfo.InputString);
                            }
                            break;
                        case HashMethod.SHA256:
                            if (hashInfo.GenerateForFile)
                            {
                                return SHA256.HashFile.CALCHASH_base64(hashInfo.InputString);
                            }
                            else if (!hashInfo.GenerateForFile)
                            {
                                return SHA256.FromString.CALCHASH_base64(hashInfo.InputString);
                            }
                            break;
                        case HashMethod.SHA384:
                            if (hashInfo.GenerateForFile)
                            {
                                return SHA384.HashFile.CALCHASH_base64(hashInfo.InputString);
                            }
                            else if (!hashInfo.GenerateForFile)
                            {
                                return SHA384.FromString.CALCHASH_base64(hashInfo.InputString);
                            }
                            break;
                        case HashMethod.SHA512:
                            if (hashInfo.GenerateForFile)
                            {
                                return SHA512.HashFile.CALCHASH_base64(hashInfo.InputString);
                            }
                            else if (!hashInfo.GenerateForFile)
                            {
                                return SHA512.FromString.CALCHASH_base64(hashInfo.InputString);
                            }
                            break;
                        case HashMethod.HMACSHA1:
                            if (hashInfo.GenerateForFile)
                            {
                                return HMACSHA1.HashFile.CALCHASH_base64(hashInfo.InputString);
                            }
                            else if (!hashInfo.GenerateForFile)
                            {
                                return HMACSHA1.FromString.CALCHASH_base64(hashInfo.InputString);
                            }
                            break;
                        case HashMethod.HMACSHA256:
                            if (hashInfo.GenerateForFile)
                            {
                                return HMACSHA256.HashFile.CALCHASH_base64(hashInfo.InputString);
                            }
                            else if (!hashInfo.GenerateForFile)
                            {
                                return HMACSHA256.FromString.CALCHASH_base64(hashInfo.InputString);
                            }
                            break;
                        case HashMethod.HMACSHA384:
                            if (hashInfo.GenerateForFile)
                            {
                                return HMACSHA384.HashFile.CALCHASH_base64(hashInfo.InputString);
                            }
                            else if (!hashInfo.GenerateForFile)
                            {
                                return HMACSHA384.FromString.CALCHASH_base64(hashInfo.InputString);
                            }
                            break;
                        case HashMethod.HMACSHA512:
                            if (hashInfo.GenerateForFile)
                            {
                                return HMACSHA512.HashFile.CALCHASH_base64(hashInfo.InputString);
                            }
                            else if (!hashInfo.GenerateForFile)
                            {
                                return HMACSHA512.FromString.CALCHASH_base64(hashInfo.InputString);
                            }
                            break;
                    }
                    break;
                default: return null;
            }
            return null;
        }
    }
}
