﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using CryptoCore.Helpers;
using AES.GCM;
using System.Security;
using RFC2898;

namespace CryptoCore.CommandHandlers.AES
{
    class GCMEncryptString
    {
        public static void EncryptString(string [] args, byte[] authData, SecureString passPhrase)
        {
            List<string> strings = new List<string>();
            if (args.Length < 4)
            {
                bool again = true;
                do
                {

                    // Ask for file input
                    Program.ColoredConsoleWrite(ConsoleColor.Magenta, "INPUT: ");
                    Program.ColoredConsoleWrite(ConsoleColor.White, "String to encrypt: ");

                    // add to list
                    strings.Add(Console.ReadLine());

                    // Ask if user wants to add another string
                    MessagePresets.WriteInput("Add another string? (Y/N): ");
                    if (Console.ReadLine() == "N" || Console.ReadLine() == "n")
                        again = false;

                } while (again);

            }
            else
            {
                // Loop for adding strings
                for(int i = 3; i < args.Length; i++)
                {
                    strings.Add(args[i]);
                }
            }
            Stopwatch sw = new Stopwatch();

            List<Task> tasks = new List<Task>();
            Console.WriteLine();

            

            passPhrase.Clear();
            // User confirmation to start operation
            MessagePresets.WriteWarning("All set! Start operation? (Y/N)");
            string input = Console.ReadLine();
            if (input == "n" || input == "N")
            {
                throw new Exception("Operation aborted");
            }

            sw.Start();

            // Derive passPhrase to key
            MessagePresets.WriteInfo("Deriving passphrase to key...");
            DeriveBytes.RunWithoutSalt(passPhrase, 256, 128, 8);
            Program.ColoredConsoleWrite(ConsoleColor.Green, "OK\n");
            Console.WriteLine();

            for(int i = 0; i < strings.Count; i++)
            {
                GCMEncryptionInfo gi = new GCMEncryptionInfo();
                gi.AuthData = authData;
                gi.NonEncPax = DeriveBytes.GetSalt();

                byte[] enc = GCM.Encrypt(Encoding.UTF8.GetBytes(strings[i]), DeriveBytes.GetKey(), gi);

                Console.WriteLine(string.Format("Encrypted string {0}:", i));
                Console.WriteLine(Convert.ToBase64String(enc));
                Console.WriteLine();
            }
            sw.Stop();
            MessagePresets.WriteSuccess("Encryption Successfull! Completed in " + sw.ElapsedMilliseconds + " ms");
            Console.WriteLine("\n");
        }
    }
}
