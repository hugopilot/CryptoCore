﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Security;
using CryptoCore.Helpers;
using RFC2898;
using AES.GCM;

namespace CryptoCore.CommandHandlers.AES
{
    public class GCMDecryptString
    {
        public static void DecryptString(string[] args, byte[] AuthData, SecureString passPhrase)
        {
            List<string> strings = new List<string>();
            if (args.Length < 4)
            {
                bool again = true;
                do
                {
                    // Ask for file input
                    Program.ColoredConsoleWrite(ConsoleColor.Magenta, "INPUT: ");
                    Program.ColoredConsoleWrite(ConsoleColor.White, "String to decrypt: ");

                    // add to list
                    strings.Add(Console.ReadLine());

                    // Ask if user wants to add another file
                    MessagePresets.WriteInput("Add another string? (Y/N): ");
                    if (Console.ReadLine() == "N" || Console.ReadLine() == "n")
                        again = false;

                } while (again);

            }
            else
            {
                for (int i = 3; i < args.Length; i++)
                {
                    strings.Add(args[i]);
                }
            }
            Stopwatch sw = new Stopwatch();

            List<Task> tasks = new List<Task>();
            Console.WriteLine();


            MessagePresets.WriteWarning("All set! Start operation? (Y/N)");
            string input = Console.ReadLine();
            if (input == "n" || input == "N")
            {
                throw new Exception("Operation aborted");
            }
            sw.Start();

            for (int C = 0; C < strings.Count; C++)
            {
                MessagePresets.WriteInfo(string.Format("Reading string {0}...", C + 1));
                byte[] PLAIN = Convert.FromBase64String(strings[C]);
                Program.ColoredConsoleWrite(ConsoleColor.Green, "OK\n");

                // Get salt from the file
                MessagePresets.WriteInfo("Getting salt from string...");
                byte[] salt = DeriveBytes.GetNonEncPaxFromString(PLAIN, 0, 8);
                Program.ColoredConsoleWrite(ConsoleColor.Green, "OK\n");

                // Derive passphrase
                MessagePresets.WriteInfo("Deriving passphrase to key...");
                DeriveBytes.RunWithSalt(passPhrase, salt, 256, 128);
                Program.ColoredConsoleWrite(ConsoleColor.Green, "OK\n");

                MessagePresets.WriteInfo(string.Format("Decrypting string {0}...", C + 1));
                tasks.Add(Task.Factory.StartNew(() =>
                {
                    GCMDecryptionInfo info = new GCMDecryptionInfo();
                    info.AuthData = AuthData;
                    info.NonEncPax_length = 8;

                    // Decrypt file
                    try
                    {
                        byte[] enc = GCM.Decrypt(PLAIN, DeriveBytes.GetKey(), info);

                        // Write to file and remove contents from memory (Not phyisiclly)
                        PLAIN = new byte[2];
                        Console.WriteLine();
                        Console.WriteLine(Encoding.UTF8.GetString(enc));
                        Console.WriteLine();
                        enc = new byte[2];
                        Program.ColoredConsoleWrite(ConsoleColor.Green, "OK\n");
                        sw.Stop();
                        MessagePresets.WriteSuccess("Decryption Successfull! Completed in " + sw.ElapsedMilliseconds + " ms");
                        Console.WriteLine("\n");
                    }
                    catch (Exception e)
                    {
                        MessagePresets.WriteError("ERROR: " + e.Message + "\n");
                    }

                }));
                Console.WriteLine();
                Task.WaitAll(tasks.ToArray());
            }
        }
    }
}
