﻿using System.Diagnostics;
using System.Text;
using System.IO;
using System;
using System.Security;
using System.Collections.Generic;
using System.Threading.Tasks;
using RFC2898;
using CryptoCore.Helpers;
using AES.GCM;


namespace CryptoCore.CommandHandlers.AES
{
    public class GCMDecryptFile
    {
        public static void DecryptFile(string[] args, byte[] authData, SecureString passPhrase)
        {
            List<string> FilePath = new List<string>(); 
            List<string> OutputFile = new List<string>();
            if (args.Length < 4)
            {
                bool again = true;
                do
                {
                    // Ask for file input
                    Program.ColoredConsoleWrite(ConsoleColor.Magenta, "INPUT: ");
                    Program.ColoredConsoleWrite(ConsoleColor.White, "Path to file?: ");
                    // add to list
                    FilePath.Add(Console.ReadLine());

                    // Ask for file output
                    Program.ColoredConsoleWrite(ConsoleColor.Magenta, "INPUT: ");
                    Program.ColoredConsoleWrite(ConsoleColor.White, "Path to output file?: ");
                    OutputFile.Add(Console.ReadLine());

                    // Ask if user wants to add another file
                    MessagePresets.WriteInput("Add another file? (Y/N): ");
                    if (Console.ReadLine() == "N" || Console.ReadLine() == "n")
                        again = false;

                } while (again);

            }
            else
            {
                for (int i = 3; i < args.Length; i++)
                {
                    if (i % 2 == 0)
                    {
                        // Is Even, for OutputFile
                        OutputFile.Add(args[i]);
                    }
                    else
                    {
                        // Is odd, for FilePath
                        FilePath.Add(args[i]);
                    }

                }
            }
            Stopwatch sw = new Stopwatch();
            sw.Start();
            List<Task> tasks = new List<Task>();
            Console.WriteLine();

            // Check if FilePath has the same length as OutputFile
            if (FilePath.Count != OutputFile.Count) { throw new OverflowException("Input file paths and output file paths don't have the same count"); }


            MessagePresets.WriteWarning("All set! Start operation? (Y/N)");
            string input = Console.ReadLine();
            if (input == "n" || input == "N")
            {
                throw new Exception("Operation aborted");
            }


            for (int C = 0; C < FilePath.Count; C++)
            {
                MessagePresets.WriteInfo(string.Format("Reading file {0}...", C + 1));
                byte[] PLAIN = File.ReadAllBytes(FilePath[C]);
                Program.ColoredConsoleWrite(ConsoleColor.Green, "OK\n");

                // Get salt from the file
                MessagePresets.WriteInfo("Getting salt from file...");
                byte[] salt = DeriveBytes.GetNonEncPaxFromString(PLAIN, 0, 8);
                Program.ColoredConsoleWrite(ConsoleColor.Green, "OK\n");

                // Derive passphrase
                MessagePresets.WriteInfo("Deriving passphrase to key...");
                DeriveBytes.RunWithSalt(passPhrase, salt, 256, 128);
                Program.ColoredConsoleWrite(ConsoleColor.Green, "OK\n");

                MessagePresets.WriteInfo(string.Format("Decrypting file {0}...", C + 1));
                tasks.Add(Task.Factory.StartNew(() =>
                {
                    GCMDecryptionInfo info = new GCMDecryptionInfo();
                    info.AuthData = authData;
                    info.NonEncPax_length = 8;

                    // Decrypt file
                    try
                    {
                        byte[] enc = GCM.Decrypt(PLAIN, DeriveBytes.GetKey(), info);

                        // Write to file and remove contents from memory (Not phyisiclly)
                        PLAIN = new byte[2];
                        File.WriteAllBytes(OutputFile[C], enc);
                        enc = new byte[2];
                        Program.ColoredConsoleWrite(ConsoleColor.Green, "OK\n");
                        sw.Stop();
                        
                    }
                    catch(Exception e)
                    {
                        MessagePresets.WriteError("ERROR: " + e.Message + "\n");
                    }
                    
                }));
                MessagePresets.WriteSuccess("Decryption Successfull! Completed in " + sw.ElapsedMilliseconds + " ms");
                Console.WriteLine("\n");
                Console.WriteLine();
                Task.WaitAll(tasks.ToArray());
            }

            
        }
    }
}
