﻿using System;
using System.Collections.Generic;
using System.Text;
using CryptoCore.CommandHandlers.GenerateHash;
using CryptoCore.Helpers;
using System.Diagnostics;

namespace CryptoCore.CommandHandlers
{
    public class gh
    {
        public static void Generate(string[] args)
        {

            // Start a stopwatch for diagonstics
            Stopwatch sw = new Stopwatch();
            sw.Start();

            // Initlize all models 
            HashInfo info = new HashInfo();
            HashMethod method = new HashMethod();
            FormatOptions format = new FormatOptions();

            // 'Compile' arguments to model information
            switch (args[1])
            {
                case "-m":
                    switch (args[2])
                    {
                        case "SHA1": method = HashMethod.SHA1; break;
                        case "SHA256": method = HashMethod.SHA256; break;
                        case "SHA384": method = HashMethod.SHA384; break;
                        case "SHA512": method = HashMethod.SHA512; break;
                        case "HMACSHA1": method = HashMethod.HMACSHA1; break;
                        case "HMACSHA256": method = HashMethod.HMACSHA256; break;
                        case "HMACSHA384": method = HashMethod.HMACSHA384; break;
                        case "HMACSHA512": method = HashMethod.HMACSHA512; break;
                    }
                    break;
                case "--file":
                case "-f": info.GenerateForFile = true;
                    info.InputString = args[2];
                    break;
                case "--string":
                case "-s": info.GenerateForFile = false;
                    info.InputString = args[2];
                    break;
                case "-F":
                    switch (args[2])
                    {
                        case "base64": format = FormatOptions.base64; break;
                        case "hex": format = FormatOptions.hex; break;
                        case "HEX": format = FormatOptions.base64; break;
                    }
                    break;
            }
            switch (args[3])
            {
                case "-m":
                    switch (args[4])
                    {
                        case "SHA1": method = HashMethod.SHA1; break;
                        case "SHA256": method = HashMethod.SHA256; break;
                        case "SHA384": method = HashMethod.SHA384; break;
                        case "SHA512": method = HashMethod.SHA512; break;
                        case "HMACSHA1": method = HashMethod.HMACSHA1; break;
                        case "HMACSHA256": method = HashMethod.HMACSHA256; break;
                        case "HMACSHA384": method = HashMethod.HMACSHA384; break;
                        case "HMACSHA512": method = HashMethod.HMACSHA512; break;
                    }
                    break;
                case "--file":
                case "-f":
                    info.GenerateForFile = true;
                    info.InputString = args[4];
                    break;
                case "--string":
                case "-s":
                    info.GenerateForFile = false;
                    info.InputString = args[4];
                    break;
                case "-F":
                    switch (args[4])
                    {
                        case "base64": format = FormatOptions.base64; break;
                        case "hex": format = FormatOptions.hex; break;
                        case "HEX": format = FormatOptions.base64; break;
                    }
                    break;
            }
            switch (args[5])
            {
                case "-m":
                    switch (args[6])
                    {
                        case "SHA1": method = HashMethod.SHA1; break;
                        case "SHA256": method = HashMethod.SHA256; break;
                        case "SHA384": method = HashMethod.SHA384; break;
                        case "SHA512": method = HashMethod.SHA512; break;
                        case "HMACSHA1": method = HashMethod.HMACSHA1; break;
                        case "HMACSHA256": method = HashMethod.HMACSHA256; break;
                        case "HMACSHA384": method = HashMethod.HMACSHA384; break;
                        case "HMACSHA512": method = HashMethod.HMACSHA512; break;
                    }
                    break;
                case "--file":
                case "-f":
                    info.GenerateForFile = true;
                    info.InputString = args[6];
                    break;
                case "--string":
                case "-s":
                    info.GenerateForFile = false;
                    info.InputString = args[6];
                    break;
                case "-F":
                    switch (args[6])
                    {
                        case "base64": format = FormatOptions.base64; break;
                        case "hex": format = FormatOptions.hex; break;
                        case "HEX": format = FormatOptions.HEX; break;
                    }
                    break;
            }

            // Final check if InputString is not null
            if(info.InputString == null) { throw new ArgumentNullException("InputString is null!"); }

            // Generate the hash
            MessagePresets.WriteInfo("Generating hash...");
            string hash = GenerateHash.GenerateHash.Generate(info, method, format);
            MessagePresets.WriteOK();

            // Write generated hash
            Console.WriteLine();
            MessagePresets.WriteInfo("Generated hash:");
            Console.WriteLine();
            MessagePresets.WriteInfo(hash);
            Console.WriteLine();
            Console.WriteLine();
            sw.Stop();

            //Write success message with elapsed time
            MessagePresets.WriteSuccess("Hash generated in " + sw.ElapsedMilliseconds + " ms");
            Console.WriteLine();
            Console.WriteLine();
        }
    }
}
