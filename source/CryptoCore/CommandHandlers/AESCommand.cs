﻿using System;
using System.Diagnostics;
using System.Text;
using System.IO;
using System.Security;
using System.Collections.Generic;
using System.Threading.Tasks;
using RFC2898;
using CryptoCore.Helpers;
using CryptoCore.CommandHandlers.AES;
using AES.GCM;

namespace CryptoCore.CommandHandlers
{
    class AESCommand
    {
        public static void EncryptionFollowup(bool encryptfile, string[] args)
        {
            try
            {
                Program.ColoredConsoleWrite(ConsoleColor.Magenta, "INPUT: ");
                Program.ColoredConsoleWrite(ConsoleColor.White, "Which mode? ");
                string mode = Console.ReadLine();
                switch (mode)
                {
                    case "GCM":
                    case "gcm":
                        bool passOK = false;
                        SecureString passPhrase = new SecureString();
                        do
                        {
                            Program.ColoredConsoleWrite(ConsoleColor.Magenta, "INPUT: ");
                            Program.ColoredConsoleWrite(ConsoleColor.White, "Passphrase: ");

                            foreach (char c in Program.ReadPassword())
                            {
                                passPhrase.AppendChar(c);
                            }
                            Program.ColoredConsoleWrite(ConsoleColor.Magenta, "INPUT: ");
                            Program.ColoredConsoleWrite(ConsoleColor.White, "Confirm Passphrase: ");
                            SecureString compare = new SecureString();
                            foreach (char c in Program.ReadPassword())
                            {
                                compare.AppendChar(c);
                            }
                            if (SecureStrings.IsEqualTo(passPhrase, compare))
                                passOK = true;
                            else
                                MessagePresets.WriteError("Passwords didn't match! \n");
                            compare.Clear();
                            compare.Dispose();
                        } while (!passOK);


                        Program.ColoredConsoleWrite(ConsoleColor.Magenta, "INPUT: ");
                        Program.ColoredConsoleWrite(ConsoleColor.White, "Authentication Data: ");
                        byte[] authData = Encoding.Default.GetBytes(Console.ReadLine());

                        if (encryptfile)
                        {
                            GCMDecryptFile.DecryptFile(args, authData, passPhrase);
                            passPhrase.Clear();
                        }
                        else if (!encryptfile)
                        {
                            GCMDecryptString.DecryptString(args, authData, passPhrase);
                            passPhrase.Clear();
                        }
                        break;
                }
            }
            catch (Exception e)
            {
                MessagePresets.WriteError(e.Message);
            }

        }

        public static void DecryptionFollowup(bool decryptFile, string[] args)
        {
            MessagePresets.WriteInput("Mode: ");
            string mode = Console.ReadLine();
            switch (mode)
            {
                case "GCM":
                case "gcm":
                    bool passOK = false;
                    SecureString passPhrase = new SecureString();
                    do
                    {
                        Program.ColoredConsoleWrite(ConsoleColor.Magenta, "INPUT: ");
                        Program.ColoredConsoleWrite(ConsoleColor.White, "Passphrase: ");
                        
                        foreach (char c in Program.ReadPassword())
                        {
                            passPhrase.AppendChar(c);
                        }
                        Program.ColoredConsoleWrite(ConsoleColor.Magenta, "INPUT: ");
                        Program.ColoredConsoleWrite(ConsoleColor.White, "Passphrase: ");
                        SecureString compare = new SecureString();
                        foreach (char c in Program.ReadPassword())
                        {
                            passPhrase.AppendChar(c);
                        }
                        if (passPhrase == compare)
                            passOK = true;
                        compare.Clear();
                        compare.Dispose();
                    } while (!passOK);
                    

                    Program.ColoredConsoleWrite(ConsoleColor.Magenta, "INPUT: ");
                    Program.ColoredConsoleWrite(ConsoleColor.White, "Authentication Data: ");
                    byte[] authData = Encoding.Default.GetBytes(Console.ReadLine());
                    
                    if (decryptFile)
                    {
                        GCMDecryptFile.DecryptFile(args, authData, passPhrase);
                        passPhrase.Clear();
                    }
                    else if (!decryptFile)
                    {
                        GCMDecryptString.DecryptString(args, authData, passPhrase);
                        passPhrase.Clear();
                    }
                    break;
            }
        }
    }
}

