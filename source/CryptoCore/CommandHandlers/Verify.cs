﻿using System;
using System.Collections.Generic;
using System.Text;
using CryptoCore.CommandHandlers.Verification;
using System.Security.Cryptography;
using CryptoCore.Helpers;

namespace CryptoCore.CommandHandlers
{
    public class VerificationFollow
    {
        public static void VerificationFollowUp(string[] args)
        {
            VerificationInfo info = new VerificationInfo();
            Method meth = new Method();

            switch (args[2])
            {
                case "SHA1":
                    meth = Method.SHA1;
                    break;
                case "SHA256":
                    meth = Method.SHA256;
                    break;
                case "SHA3":
                case "SHA384":
                    meth = Method.SHA384;
                    break;
                case "SHA512":
                    meth = Method.SHA512;
                    break;
                case "HMACSHA1":
                    meth = Method.HMACSHA1;
                    break;
                case "HMACSHA2":
                    meth = Method.HMACSHA256;
                    break;
                case "HMACSHA3":
                case "HMACSHA384":
                    meth = Method.HMACSHA384;
                    break;
                case "HMACSHA512":
                    meth = Method.HMACSHA512;
                    break;
                default: throw new Exception("Bad method");
            }
            switch (args[3])
            {
                case "-f":
                case "--file":
                    info.VerifyFile = true;
                    info.FilePath = args[4];
                    break;
                case "-s":
                case "--string":
                    info.VerifyFile = false;
                    info.StringToVerify = args[4];
                    break;
                case "-h":
                case "--hash":
                    info.Hash = args[4];
                    break;
            }
            switch (args[5])
            {
                case "-f":
                case "--file":
                    info.VerifyFile = true;
                    info.FilePath = args[6];
                    break;
                case "-s":
                case "--string":
                    info.VerifyFile = false;
                    info.StringToVerify = args[6];
                    break;
                case "-h":
                case "--hash":
                    info.Hash = args[6];
                    break;
            }

            // Verify
            MessagePresets.WriteInfo("Verifying...");
            AfterVerificationInfo aInfo = Verification.Verification.Verify(info, meth);
            MessagePresets.WriteOK();
            Console.WriteLine();
            Console.WriteLine();

            // Write success message when valid
            if (aInfo.valid)
            {
                if (info.VerifyFile)
                {
                    MessagePresets.WriteSuccess("The file is valid.");
                    Console.WriteLine();
                    Console.WriteLine();
                    MessagePresets.WriteInfo("Given hash: " + aInfo.GivenHash);
                    Console.WriteLine();
                    MessagePresets.WriteInfo("Calculated hash: " + aInfo.ExternalHash);
                    Console.WriteLine();
                    Console.WriteLine();
                }
                else if (!info.VerifyFile)
                {
                    MessagePresets.WriteSuccess("The string is valid.");
                    Console.WriteLine();
                    Console.WriteLine();
                    MessagePresets.WriteInfo("Given hash: " + aInfo.GivenHash);
                    Console.WriteLine();
                    MessagePresets.WriteInfo("Calculated hash: " + aInfo.ExternalHash);
                    Console.WriteLine();
                    Console.WriteLine();
                }
            }
            // Write error when invalid
            else if (!aInfo.valid)
            {
                if (info.VerifyFile)
                {
                    MessagePresets.WriteError("The file is invalid!");
                    Console.WriteLine();
                    Console.WriteLine();
                    MessagePresets.WriteInfo("Given hash: " + aInfo.GivenHash);
                    Console.WriteLine();
                    MessagePresets.WriteInfo("Calculated hash: " + aInfo.ExternalHash);
                    Console.WriteLine();
                    Console.WriteLine();
                }
                if (!info.VerifyFile)
                {
                    MessagePresets.WriteError("The string is invalid!");
                    Console.WriteLine();
                    Console.WriteLine();
                    MessagePresets.WriteInfo("Given hash: " + aInfo.GivenHash);
                    Console.WriteLine();
                    MessagePresets.WriteInfo("Calculated hash: " + aInfo.ExternalHash);
                    Console.WriteLine();
                    Console.WriteLine();
                }
            }
        }

    }
}
