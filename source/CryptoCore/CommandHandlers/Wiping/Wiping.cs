﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using SecureIO;
using CryptoCore.Helpers;
using System.Runtime.InteropServices;

namespace CryptoCore.CommandHandlers
{
    public static class Wiping
    {
        public static void WipeFileFollowup(string[] args)
        {
            List<string> paths = new List<string>();
            // Ask for file paths if there are no arguments given
            if(args.Length < 3)
            {
                bool add = true;
                do
                {
                    MessagePresets.WriteInput("Path to file: ");
                    paths.Add(Console.ReadLine());

                    MessagePresets.WriteInput("Add another file? (Y/N) ");
                    string input = Console.ReadLine();
                    if (input == "N" || input == "n")
                    {
                        add = false;
                    }
                } while (add);
                
            }
            for (int i = 2; i < args.Length; i++)
            {
                paths.Add(args[i]);
            }
            MessagePresets.WriteWarning("Confirm your operation by entering 'C'! Files will not be recoverable! ");
            if(Console.ReadLine() == "C")
            {
                foreach(string path in paths)
                {
                    MessagePresets.WriteInfo(string.Format("Wiping {0}...", Path.GetFileName(path)));
                    Wipe.WipeFile(path, 3);
                    Program.ColoredConsoleWrite(ConsoleColor.Green, "OK\n");
                }
                MessagePresets.WriteSuccess("All files have been wiped!");
                Console.WriteLine();
            }
        }


        /// <summary>
        /// Wipes avaiable disk space
        /// </summary>
        /// <param name="args">arguments from console</param>
        public static void WipeAvailableSpace(string[] args)
        {
            // Get Drive path/letter
            string Drive = args[2];

            // Check if drive exists
            MessagePresets.WriteInfo("Searching for drive...");
            if (!Directory.Exists(Drive))
            {
                throw new DirectoryNotFoundException("Drive " + Drive + " not found!");
            }
            MessagePresets.WriteOK();
            Console.WriteLine();

            // Get avaiable diskspace
            MessagePresets.WriteInfo("Getting available disk space...");
            DriveInfo d = new DriveInfo(Drive);
            long s = d.AvailableFreeSpace;
            MessagePresets.WriteOK();

            // Print number of bytes available
            Console.WriteLine();
            MessagePresets.WriteInfo(string.Format("{0, 15} bytes free", s));
            Console.WriteLine();

            //Create a random file to fill the disk space with
            MessagePresets.WriteInfo("Creating file to fill space...");
            string path = Path.Combine(Drive, RandomTypes.RandomString(8) + "." + RandomTypes.RandomString(3));
            using(FileStream fs = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None))
            {
                fs.SetLength(s);
            }           

            // Get number of times disk should be over written
            int t = 1;
            if(args.Length < 4)
            {
                // t will be left 1
            }
            else
            {
                t = Convert.ToInt16(args[4]);
            }
            MessagePresets.WriteOK();
            Console.WriteLine();

            // Wipe the file/available disk space
            MessagePresets.WriteInfo("Wiping file...");
            Wipe.WipeFile(path, t);
            MessagePresets.WriteOK();
            Console.WriteLine();

            // Print success message
            MessagePresets.WriteSuccess("Available diskspace was wiped!");
            Console.WriteLine();
        }
        public static void WipeDisk(string[] args)
        {
            // Main settings
            bool nullDrive = false;
            string drive = args[2];

            // Check if drive should be nulled
            if(args.Length > 3)
            {
                if (args[3] == "-n")
                    nullDrive = true;
            }
            // Check if drive is present
            MessagePresets.WriteInfo("Searching for drive...");
            if (!Directory.Exists(drive))
            {
                throw new DirectoryNotFoundException("Drive " + drive + " not found!");
            }

            if (nullDrive)
            {
                MessagePresets.WriteOK();
                Console.WriteLine();
                MessagePresets.WriteWarning("Nulling a drive can take up hours or days to complete, depending on it's size.");
                Console.WriteLine();
                MessagePresets.WriteWarning("You are one step removed of completely wiping your drive! Confirm your request by entering 'C'! Contents will not be recoverable! ");
                if (Console.ReadLine() == "C")
                {
                    // Step 1: Get all files and wipe
                    // Step 1: Get all files and wipe
                    MessagePresets.WriteInput("Creating file tree...");
                    string[] allFiles = DirSearch(drive);
                    MessagePresets.WriteOK();
                    Console.WriteLine();
                    foreach (string file in allFiles)
                    {
                        MessagePresets.WriteInfo(string.Format("Wiping {0}...", file));
                        try
                        {
                            Wipe.WipeFile(file, 1);
                        }
                        catch (UnauthorizedAccessException)
                        {
                            ForceOwnage(file);
                            Wipe.WipeFile(file, 1);

                        }
                        MessagePresets.WriteOK();
                        Console.WriteLine();
                    }

                    // Delete directories
                    DirectoryInfo di = new DirectoryInfo(drive);
                    foreach (DirectoryInfo dir in di.GetDirectories())
                    {
                        dir.Delete(true);
                    }

                    MessagePresets.WriteSuccess("All files were wiped! (1/3)");
                    Console.WriteLine();
                    // Step 2: Overwrite full disk with Junk

                    // Get avaiable diskspace
                    MessagePresets.WriteInfo("Getting available disk space...");
                    DriveInfo d = new DriveInfo(drive);
                    long s = d.AvailableFreeSpace;
                    MessagePresets.WriteOK();

                    // Print number of bytes available
                    Console.WriteLine();
                    MessagePresets.WriteInfo(string.Format("{0, 15} bytes free", s));
                    Console.WriteLine();

                    //Create a random file to fill the disk space with
                    MessagePresets.WriteInfo("Creating file to fill space...");
                    string path = Path.Combine(drive, RandomTypes.RandomString(8) + "." + RandomTypes.RandomString(3));
                    using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None))
                    {
                        fs.SetLength(s);
                    }
                    // Wipe the file/available disk space
                    Console.WriteLine();
                    MessagePresets.WriteInfo("Wiping file...");
                    Wipe.WipeFile(path, 1);
                    MessagePresets.WriteOK();
                    Console.WriteLine();

                    // Print success message
                    MessagePresets.WriteSuccess("Available diskspace was wiped! (2/3)");
                    Console.WriteLine();

                    // Step 3: Nulling drive

                    // Create a new file path
                    string path2 = Path.Combine(drive, RandomTypes.RandomString(8) + "." + RandomTypes.RandomString(3));

                    // Null the drive
                    MessagePresets.WriteInfo("Nulling drive...");
                    NullFile(path2, s);
                    // Delete file from MFT without wiping
                    File.Delete(path2);
                    MessagePresets.WriteOK();
                    Console.WriteLine();

                    // Print success message
                    MessagePresets.WriteSuccess("Drive nulled! (3/3)");
                    Console.WriteLine();
                    MessagePresets.WriteSuccess("Operation completed with success!");
                    Console.WriteLine();
                    MessagePresets.WriteWarning("On NTFS and FAT drives, it is recommended to reformat the drive before using it again");
                    Console.WriteLine();
                }
            }
            else if (!nullDrive)
            {
                MessagePresets.WriteOK();
                Console.WriteLine();
                MessagePresets.WriteWarning("It's recommended to run this in admin/sudo mode to prevent unauthorized access \n");
                MessagePresets.WriteWarning("You are one step removed of completely wiping your drive! Confirm your request by entering 'C'! Contents will not be recoverable! ");
                if (Console.ReadLine() == "C")
                {
                    // Step 1: Get all files and wipe
                    MessagePresets.WriteInput("Creating file tree...");
                    string[] allFiles = DirSearch(drive);
                    MessagePresets.WriteOK();
                    Console.WriteLine();
                    foreach (string file in allFiles)
                    {
                        MessagePresets.WriteInfo(string.Format("Wiping {0}...", file));
                        try
                        {
                            Wipe.WipeFile(file, 1);
                        }
                        catch (UnauthorizedAccessException)
                        {
                            ForceOwnage(file);
                            Wipe.WipeFile(file, 1);

                        }
                        MessagePresets.WriteOK();
                        Console.WriteLine();
                    }

                    // Delete directories
                    DirectoryInfo di = new DirectoryInfo(drive);
                    foreach (DirectoryInfo dir in di.GetDirectories())
                    {
                        dir.Delete(true);
                    }
                    MessagePresets.WriteSuccess("All files were wiped! (1/2)");
                    Console.WriteLine();
                    // Step 2: Overwrite full disk with Junk

                    // Get avaiable diskspace
                    MessagePresets.WriteInfo("Getting available disk space...");
                    DriveInfo d = new DriveInfo(drive);
                    long s = d.AvailableFreeSpace;
                    MessagePresets.WriteOK();

                    // Print number of bytes available
                    Console.WriteLine();
                    MessagePresets.WriteInfo(string.Format("{0, 15} bytes free", s));
                    Console.WriteLine();

                    //Create a random file to fill the disk space with
                    MessagePresets.WriteInfo("Creating file to fill space...");
                    string path = Path.Combine(drive, RandomTypes.RandomString(8) + "." + RandomTypes.RandomString(3));
                    using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None))
                    {
                        fs.SetLength(s);
                    }
                    Console.WriteLine();

                    // Wipe the file/available disk space
                    Console.WriteLine();
                    MessagePresets.WriteInfo("Wiping file...");
                    Wipe.WipeFile(path, 2);
                    MessagePresets.WriteOK();
                    Console.WriteLine();

                    // Print success message
                    MessagePresets.WriteSuccess("Available diskspace was wiped! (2/2)");
                    Console.WriteLine();
                    MessagePresets.WriteSuccess("Operation completed with success!");
                    Console.WriteLine();
                    MessagePresets.WriteWarning("On NTFS and FAT drives, it is recommended to reformat the drive before using it again");
                    Console.WriteLine();
                }
            }
        }
        private static void NullFile(string path, long length)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None))
            {
                TimeSpan expectT = TimeSpan.FromSeconds((length / 100000000)*3600);
                fs.SetLength(length);
                for(long i = 0; i < fs.Length; i++)
                {
                    fs.Position = i;
                    fs.WriteByte(0);
                    Console.Write(string.Format("\r{0}/{1} bytes nulled...{2} elapsed...{3} predicted...", i, fs.Length, sw.Elapsed, expectT.ToString(@"d\d\,\ hh\:mm\:ss")));
                }
            }
            sw.Stop();
        }
        private static string[] DirSearch(string sDir)
        {
            try
            {
                return Directory.GetFiles(sDir, "*.*", SearchOption.AllDirectories);
            }
            catch (UnauthorizedAccessException ue)
            {
                // Get file name it can't access
                string[] fn = ue.Message.Split('\'', '\'');
                MessagePresets.WriteError(string.Format("Can't access {0}, trying to force ownership...", fn[1]));
                Console.WriteLine();
                ForceOwnage(fn[1]);
                DirSearch(sDir);
            }
            return null;
        }
        static string usrName = null;
        private static void ForceOwnage(string filepath)
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                // Initalize cacls and takeown processes
                Process cacls = new Process();
                Process takeown = new Process();

                // prompt user to enter username
                if (usrName == null)
                {
                    Console.WriteLine();
                    MessagePresets.WriteInput("Enter username for verification: ");
                    usrName = Console.ReadLine();
                }

                // Set start info
                takeown.StartInfo.FileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86), "takeown.exe");
                takeown.StartInfo.Arguments = string.Format("/F \"{0}\"", filepath);
                takeown.StartInfo.Verb = "runas";

                cacls.StartInfo.FileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86), "cacls.exe");
                cacls.StartInfo.Arguments = string.Format("\"{0}\" /E /G {1}:F", filepath, usrName);
                cacls.StartInfo.Verb = "runas";

                // Run programs
                takeown.Start();
                takeown.WaitForExit();

                cacls.Start();
                cacls.WaitForExit();
            }
        }
    }
}
