﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CryptoCore.CommandHandlers.Verification
{
    public struct VerificationInfo
    {
        /// <summary>
        /// Verification method
        /// </summary>

        public bool VerifyFile { get; set; }

        public string FilePath { get; set; }

        public string StringToVerify { get; set; }

        public string Hash { get; set; }
    }
    public enum Method
    {
        SHA1,
        SHA256,
        SHA384,
        SHA512,
        HMACSHA1,
        HMACSHA256,
        HMACSHA384,
        HMACSHA512
    }

}
