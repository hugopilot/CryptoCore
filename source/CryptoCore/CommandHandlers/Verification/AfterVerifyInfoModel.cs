﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CryptoCore.CommandHandlers.Verification
{
    public struct AfterVerificationInfo
    {
        /// <summary>
        /// True if the file/string hash is equal with the given hash
        /// </summary>
        public bool valid { get; set; }

        /// <summary>
        /// The given hash by the user
        /// </summary>
        public string GivenHash { get; set; }

        /// <summary>
        /// The calculated hash of the string/file
        /// </summary>
        public string ExternalHash { get; set; }
    }
}
