﻿using System;
using System.Collections.Generic;
using System.Text;
using SHAhelper;
using CryptoCore.Helpers;

namespace CryptoCore.CommandHandlers.Verification
{
    public class Verification
    {

        /// <summary>
        /// Verifies a file or string via a specified verification mode
        /// </summary>
        /// <param name="info">All the required info in this struct container</param>
        /// <param name="method">The desired method of verifying</param>
        /// <returns>Returns true if valid, false if invalid</returns>
        public static AfterVerificationInfo Verify(VerificationInfo info, Method method)
        {
            AfterVerificationInfo aInfo = new AfterVerificationInfo();
            aInfo.GivenHash = info.Hash;
            // Select which method should be used
            switch (method)
            {
                case Method.SHA1:
                    if (info.VerifyFile)
                    {
                        if (StringTypeCheck.IsBase64(info.Hash))
                        {
                            // Calculate the hash of the file/string
                            string fh = SHA1.HashFile.CALCHASH_base64(info.FilePath);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                        else if (StringTypeCheck.IsHex(info.Hash))
                        {
                            string fh = SHA1.HashFile.CALCHASH_hex(info.FilePath);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                        else if (StringTypeCheck.IsHEX(info.Hash))
                        {
                            string fh = SHA1.HashFile.CALCHASH_HEX(info.FilePath);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                    }
                    else if (!info.VerifyFile)
                    {
                        if (StringTypeCheck.IsBase64(info.Hash))
                        {
                            // Calculate the hash of the file/string
                            string fh = SHA1.FromString.CALCHASH_base64(info.StringToVerify);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                        else if (StringTypeCheck.IsHex(info.Hash))
                        {
                            string fh = SHA1.FromString.CALCHASH_hex(info.StringToVerify);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                        else if (StringTypeCheck.IsHEX(info.Hash))
                        {
                            string fh = SHA1.FromString.CALCHASH_HEX(info.StringToVerify);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                    }
                    
                    break;
                case Method.SHA256:
                    if (info.VerifyFile)
                    {
                        if (StringTypeCheck.IsBase64(info.Hash))
                        {
                            // Calculate the hash of the file/string
                            string fh = SHA256.HashFile.CALCHASH_base64(info.FilePath);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                        else if (StringTypeCheck.IsHex(info.Hash))
                        {
                            string fh = SHA256.HashFile.CALCHASH_hex(info.FilePath);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                        else if (StringTypeCheck.IsHEX(info.Hash))
                        {
                            string fh = SHA256.HashFile.CALCHASH_HEX(info.FilePath);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                    }
                    else if (!info.VerifyFile)
                    {
                        if (StringTypeCheck.IsBase64(info.Hash))
                        {
                            // Calculate the hash of the file/string
                            string fh = SHA256.FromString.CALCHASH_base64(info.StringToVerify);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                        else if (StringTypeCheck.IsHex(info.Hash))
                        {
                            string fh = SHA256.FromString.CALCHASH_hex(info.StringToVerify);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                        else if (StringTypeCheck.IsHEX(info.Hash))
                        {
                            string fh = SHA256.FromString.CALCHASH_HEX(info.StringToVerify);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                    }
                    break;
                case Method.SHA384:
                    if (info.VerifyFile)
                    {
                        if (StringTypeCheck.IsBase64(info.Hash))
                        {
                            // Calculate the hash of the file/string
                            string fh = SHA384.HashFile.CALCHASH_base64(info.FilePath);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                        else if (StringTypeCheck.IsHex(info.Hash))
                        {
                            string fh = SHA384.HashFile.CALCHASH_hex(info.FilePath);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                        else if (StringTypeCheck.IsHEX(info.Hash))
                        {
                            string fh = SHA384.HashFile.CALCHASH_HEX(info.FilePath);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                    }
                    else if (!info.VerifyFile)
                    {
                        if (StringTypeCheck.IsBase64(info.Hash))
                        {
                            // Calculate the hash of the file/string
                            string fh = SHA384.FromString.CALCHASH_base64(info.StringToVerify);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                        else if (StringTypeCheck.IsHex(info.Hash))
                        {
                            string fh = SHA384.FromString.CALCHASH_hex(info.StringToVerify);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                        else if (StringTypeCheck.IsHEX(info.Hash))
                        {
                            string fh = SHA384.FromString.CALCHASH_HEX(info.StringToVerify);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                    }
                    break;
                case Method.SHA512:
                    if (info.VerifyFile)
                    {
                        if (StringTypeCheck.IsBase64(info.Hash))
                        {
                            // Calculate the hash of the file/string
                            string fh = SHA512.HashFile.CALCHASH_base64(info.FilePath);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                        else if (StringTypeCheck.IsHex(info.Hash))
                        {
                            string fh = SHA512.HashFile.CALCHASH_hex(info.FilePath);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                        else if (StringTypeCheck.IsHEX(info.Hash))
                        {
                            string fh = SHA512.HashFile.CALCHASH_HEX(info.FilePath);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                    }
                    else if (!info.VerifyFile)
                    {
                        if (StringTypeCheck.IsBase64(info.Hash))
                        {
                            // Calculate the hash of the file/string
                            string fh = SHA512.FromString.CALCHASH_base64(info.StringToVerify);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                        else if (StringTypeCheck.IsHex(info.Hash))
                        {
                            string fh = SHA512.FromString.CALCHASH_hex(info.StringToVerify);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                        else if (StringTypeCheck.IsHEX(info.Hash))
                        {
                            string fh = SHA512.FromString.CALCHASH_HEX(info.StringToVerify);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                    }
                    break;
                case Method.HMACSHA1:
                    if (info.VerifyFile)
                    {
                        if (StringTypeCheck.IsBase64(info.Hash))
                        {
                            // Calculate the hash of the file/string
                            string fh = HMACSHA1.HashFile.CALCHASH_base64(info.FilePath);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                        else if (StringTypeCheck.IsHex(info.Hash))
                        {
                            string fh = HMACSHA1.HashFile.CALCHASH_hex(info.FilePath);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                        else if (StringTypeCheck.IsHEX(info.Hash))
                        {
                            string fh = HMACSHA1.HashFile.CALCHASH_HEX(info.FilePath);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                    }
                    else if (!info.VerifyFile)
                    {
                        if (StringTypeCheck.IsBase64(info.Hash))
                        {
                            // Calculate the hash of the file/string
                            string fh = HMACSHA1.FromString.CALCHASH_base64(info.StringToVerify);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                        else if (StringTypeCheck.IsHex(info.Hash))
                        {
                            string fh = HMACSHA1.FromString.CALCHASH_hex(info.StringToVerify);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                        else if (StringTypeCheck.IsHEX(info.Hash))
                        {
                            string fh = HMACSHA1.FromString.CALCHASH_HEX(info.StringToVerify);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                    }
                    break;
                case Method.HMACSHA256:
                    if (info.VerifyFile)
                    {
                        if (StringTypeCheck.IsBase64(info.Hash))
                        {
                            // Calculate the hash of the file/string
                            string fh = HMACSHA256.HashFile.CALCHASH_base64(info.FilePath);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                        else if (StringTypeCheck.IsHex(info.Hash))
                        {
                            string fh = HMACSHA256.HashFile.CALCHASH_hex(info.FilePath);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                        else if (StringTypeCheck.IsHEX(info.Hash))
                        {
                            string fh = HMACSHA256.HashFile.CALCHASH_HEX(info.FilePath);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                    }
                    else if (!info.VerifyFile)
                    {
                        if (StringTypeCheck.IsBase64(info.Hash))
                        {
                            // Calculate the hash of the file/string
                            string fh = HMACSHA256.FromString.CALCHASH_base64(info.StringToVerify);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                        else if (StringTypeCheck.IsHex(info.Hash))
                        {
                            string fh = HMACSHA256.FromString.CALCHASH_hex(info.StringToVerify);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                        else if (StringTypeCheck.IsHEX(info.Hash))
                        {
                            string fh = HMACSHA256.FromString.CALCHASH_HEX(info.StringToVerify);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                    }
                    break;
                case Method.HMACSHA384:
                    if (info.VerifyFile)
                    {
                        if (StringTypeCheck.IsBase64(info.Hash))
                        {
                            // Calculate the hash of the file/string
                            string fh = HMACSHA384.HashFile.CALCHASH_base64(info.FilePath);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                        else if (StringTypeCheck.IsHex(info.Hash))
                        {
                            string fh = HMACSHA384.HashFile.CALCHASH_hex(info.FilePath);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                        else if (StringTypeCheck.IsHEX(info.Hash))
                        {
                            string fh = HMACSHA384.HashFile.CALCHASH_HEX(info.FilePath);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                    }
                    else if (!info.VerifyFile)
                    {
                        if (StringTypeCheck.IsBase64(info.Hash))
                        {
                            // Calculate the hash of the file/string
                            string fh = HMACSHA384.FromString.CALCHASH_base64(info.StringToVerify);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                        else if (StringTypeCheck.IsHex(info.Hash))
                        {
                            string fh = HMACSHA384.FromString.CALCHASH_hex(info.StringToVerify);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                        else if (StringTypeCheck.IsHEX(info.Hash))
                        {
                            string fh = HMACSHA384.FromString.CALCHASH_HEX(info.StringToVerify);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                    }
                    break;
                case Method.HMACSHA512:
                    if (info.VerifyFile)
                    {
                        if (StringTypeCheck.IsBase64(info.Hash))
                        {
                            // Calculate the hash of the file/string
                            string fh = HMACSHA512.HashFile.CALCHASH_base64(info.FilePath);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                        else if (StringTypeCheck.IsHex(info.Hash))
                        {
                            string fh = HMACSHA512.HashFile.CALCHASH_hex(info.FilePath);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                        else if (StringTypeCheck.IsHEX(info.Hash))
                        {
                            string fh = HMACSHA512.HashFile.CALCHASH_HEX(info.FilePath);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                    }
                    else if (!info.VerifyFile)
                    {
                        if (StringTypeCheck.IsBase64(info.Hash))
                        {
                            // Calculate the hash of the file/string
                            string fh = HMACSHA512.FromString.CALCHASH_base64(info.StringToVerify);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                        else if (StringTypeCheck.IsHex(info.Hash))
                        {
                            string fh = HMACSHA512.FromString.CALCHASH_hex(info.StringToVerify);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                        else if (StringTypeCheck.IsHEX(info.Hash))
                        {
                            string fh = HMACSHA512.FromString.CALCHASH_HEX(info.StringToVerify);
                            aInfo.ExternalHash = fh;
                            if (fh != info.Hash)
                            {
                                aInfo.valid = false;
                            }
                            else if (fh == info.Hash)
                            {
                                aInfo.valid = true;
                            }
                            return aInfo;
                        }
                    }
                    break;
                default: throw new Exception("Wrong!");
                    
            }
            aInfo.valid = false;
            return aInfo;
        }
    }
}
