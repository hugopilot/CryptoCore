﻿using System;
using System.Collections.Generic;
using System.Security;
using System.Security.Cryptography;
namespace RFC2898
{
    public class DeriveBytes
    {
        private static byte[] IV;
        private static byte[] KEY;
        private static byte[] SALT;
        public static void RunWithoutSalt(SecureString PASSPHRASE, int KEYSIZE, int BLOCKSIZE, int SALTSIZE)
        {
            IntPtr unmanagedString = IntPtr.Zero;
            unmanagedString = System.Runtime.InteropServices.Marshal.SecureStringToGlobalAllocUnicode(PASSPHRASE);
            using (var rfc = new Rfc2898DeriveBytes(System.Runtime.InteropServices.Marshal.PtrToStringUni(unmanagedString), SALTSIZE, 10000))
            {
                SALT = rfc.Salt;
                KEY = rfc.GetBytes(KEYSIZE / 8);
                IV = rfc.GetBytes(12);
            }
        }
        public static void RunWithSalt(SecureString PASSPHRASE, byte[] SALT, int KEYSIZE, int BLOCKSIZE)
        {
            IntPtr unmanagedString = IntPtr.Zero;
            unmanagedString = System.Runtime.InteropServices.Marshal.SecureStringToGlobalAllocUnicode(PASSPHRASE);
            using (var rfc = new Rfc2898DeriveBytes(System.Runtime.InteropServices.Marshal.PtrToStringUni(unmanagedString), SALT, 10000))
            {
                KEY = rfc.GetBytes(KEYSIZE / 8);
                IV = rfc.GetBytes(12);
            }
        }
        public static byte[] GetNonEncPaxFromString(byte[] CHIPHER_TEXT, int OFFSET, int NONENCPAX_LENGTH)
        {
            List<byte> dynamicList = new List<byte>();
            for (int i = OFFSET; i < NONENCPAX_LENGTH; i++)
            {
                dynamicList.Add(CHIPHER_TEXT[i]);
            }
            return dynamicList.ToArray();
        }
        public static byte[] GetIV()
        {
            return IV;
        }
        public static byte[] GetKey()
        {
            return KEY;
        }
        public static byte[] GetSalt()
        {
            return SALT;
        }
    }
}

