# CryptoCore
A simple, cross-platform, console application using .NET Core to access a lot of cryptographic standards.

![Travis CI log](https://travis-ci.org/hugopilot/CryptoCore.svg?branch=master)

# Table of contents
1. Description
2. Downloads
3. Known issues and limitations

# Description
CryptoCore is a CLI application based on .NET Core 2.0, thus cross-platform on Windows and Unix systems. 
It will be used as a hub to access multiple cryptograpic tools. Like: AES, PGP, SHA, MDx and more to follow

**NOTE:** This program is currently a work in progress. Bugs may occur. Some of the cryptographic functions are not verified by professional cryptographists, and can not be considered completely safe. Using this program for storing sensitive data is _**at your own risk**_.

# Downloads
You can download the app [here](https://github.com/hugopilot/CryptoCore/releases), or you can [compile from source](https://github.com/hugopilot/CryptoCore/blob/master/source/Compile%20Tutorial.md)

# Known issues and limitations
Some limitations are known:
1. AES File Encryption has a file limit of 2 GB, caused by .NET Core framework.
2. HMACSHA functions do not work in some OS'ses, caused by .NET Core framework.

Current functionality:
- AES GCM File Encryption/Decryption by passphrase
- Wiping files from hard drive
- Wiping available disk space from drive
- Verifying files/strings by hash
- Generate hashes (SHA) for strings and files
